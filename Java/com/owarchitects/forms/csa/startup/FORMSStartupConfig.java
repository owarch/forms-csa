
///////////////////////////////////////////////////////////////////////////////////
//                                                                               //
// PROGRAM:  FORMSTomcatServer.java                                              //     
//                                                                               //
// PURPOSE:  This program provides an interface to start/stop/restart the FORMS  //
//    CSA Tomcat Server.                                                         //
//                                                                               //
///////////////////////////////////////////////////////////////////////////////////

package com.owarchitects.forms.csa.startup;

import java.io.*;
import java.net.*;
import java.util.*;
import org.apache.commons.io.FileUtils.*;
import org.apache.commons.lang.ArrayUtils.*;
import HTTPClient.*;
import org.apache.commons.lang.StringUtils;


public class FORMSStartupConfig {

    // Read Startup.conf file
    public static HashMap readConfig(String filepath) {
       try {
          HashMap rm=new HashMap();
          BufferedReader in = new BufferedReader(new FileReader(filepath));
          String line;
          while((line = in.readLine()) != null) {
             if (StringUtils.contains(line.toLowerCase(),"keystore:")) {
                rm.put("keystore",StringUtils.trim(StringUtils.substringAfter(line,"keystore:")));
             } else if (StringUtils.contains(line.toLowerCase(),"keypass:")) {
                rm.put("keypass",StringUtils.trim(StringUtils.substringAfter(line,"keypass:")));
             } else if (StringUtils.contains(line.toLowerCase(),"hostallow:")) {
                rm.put("hostallow",StringUtils.trim(StringUtils.substringAfter(line,"hostallow:")));
             } else if (StringUtils.contains(line.toLowerCase(),"httpport:")) {
                rm.put("httpport",StringUtils.trim(StringUtils.substringAfter(line,"httpport:")));
             } else if (StringUtils.contains(line.toLowerCase(),"httpsport:")) {
                rm.put("httpsport",StringUtils.trim(StringUtils.substringAfter(line,"httpsport:")));
             }
             
          }
          return rm;
       } catch (Exception e) {   
          return null;
       } 
    }

}

