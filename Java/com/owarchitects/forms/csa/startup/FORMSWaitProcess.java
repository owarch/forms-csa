
/////////////////////////////////////////////////////
//                                                 //
// PROGRAM:  FORMSWaitProcess.java                 //     
//                                                 //
// PURPOSE:  Sleep until have connectivity on 8080 //
//                                                 //
/////////////////////////////////////////////////////

package com.owarchitects.forms.csa.startup;

import java.io.*;
import java.net.*;
import java.util.*;
import HTTPClient.*;

public class FORMSWaitProcess {

   static String javahome=System.getProperty("java.home");
   static String appBase=javahome.substring(0,javahome.lastIndexOf(File.separator));


   public static void main( String[] args ) {
      new FORMSWaitProcess();
   }

   FORMSWaitProcess() {
  
      int httpPort;
      int httpsPort;
      HashMap m=FORMSStartupConfig.readConfig(appBase + File.separator + "Startup" + File.separator + "Startup.conf");
      try {
         httpPort=new Integer((String)m.get("httpport")).intValue();
         httpsPort=new Integer((String)m.get("httpsport")).intValue();
      } catch (Exception e) {
         httpPort=8080;
         httpsPort=8443;
      }

      for (int i=1; i<=100; i++) {
         try {
            CookieModule.setCookiePolicyHandler(null);
            HTTPConnection c = new HTTPConnection("localhost",httpPort);
            c.setTimeout(1000);
            HTTPResponse r = c.Get("/");
            int response=r.getStatusCode();
            break;
         } catch (java.io.InterruptedIOException e1) {
            try { Thread.sleep(1000); } catch (Exception e2) { }
         } catch (Exception e1) {
            try { Thread.sleep(1000); } catch (Exception e2) { }
         }
      }
   }

}

