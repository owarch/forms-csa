
/////////////////////////////////////////////////////////////////////////////
//                                                                         //
// PROGRAM:  FORMSTomcatServer.java                                        //     
//                                                                         //
// PURPOSE:  This program provides an interface to start/stop/restart the  //
//   FORMS CSA Tomcat Server.                                              //
//                                                                         //
/////////////////////////////////////////////////////////////////////////////

package com.owarchitects.forms.csa.startup;

import org.apache.catalina.Context;
import org.apache.catalina.Engine;
import org.apache.catalina.Host;
import org.apache.catalina.Session;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.realm.MemoryRealm;
import org.apache.catalina.startup.Embedded;
import org.apache.tomcat.util.IntrospectionUtils;
import org.apache.catalina.valves.RemoteAddrValve;
import org.apache.catalina.Valve;
import org.apache.catalina.Pipeline;
import org.apache.catalina.LifecycleListener;

import java.io.*;
import java.net.*;
import com.oreilly.tomcat.valve.BadInputValve;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;
import java.awt.*;
import java.util.Vector;
import java.lang.Thread;
import javax.crypto.*;
import javax.crypto.spec.*;
import java.security.*;
import java.security.spec.*;
import java.util.*;
//import com.sas.sasserver.sclfuncs.*;
//import com.sas.sasserver.dataset.*;
//import com.sas.rmi.*;
//mport com.sas.sasserver.submit.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FileUtils.*;
import org.apache.commons.lang.ArrayUtils.*;
import snoozesoft.systray4j.*;
import org.apache.commons.net.ftp.*;
import HTTPClient.*;
import org.apache.commons.lang.StringUtils;


public class FORMSTomcatServer extends JFrame implements ActionListener, KeyListener, SysTrayMenuListener
{

    ///////////////////////////// 
    // INITIALIZE CLASS FIELDS //
    ///////////////////////////// 

    static String javahome=System.getProperty("java.home");
    static String appBase=javahome.substring(0,javahome.lastIndexOf(File.separator));

    // frame dimension
    static final int INIT_WIDTH    = 520;
    static final int INIT_HEIGHT   = 260;

    String toolTip = "FORMS CSA Tomcat Server";
    String newUpdateTime=null;

    // create icons
    final SysTrayMenuIcon[] icons =

    {
        // the extension can be omitted
        //new SysTrayMenuIcon( getClass().getResource( "FORMS.ico" ) ),
        //new SysTrayMenuIcon( getClass().getResource( "FORMS.ico" ) ) 
        new SysTrayMenuIcon( appBase + File.separator + "Startup" + File.separator + "FORMS.ico" ),
        new SysTrayMenuIcon( appBase + File.separator + "Startup" + File.separator + "FORMS.ico" ) 

    };

    SysTrayMenu menu;
    int currentIndexIcon;
    int currentIndexTooltip;

    java.awt.Container c = getContentPane();

    Color bgColor=new Color(34,51,102);

    //JLabel splashlabel = new JLabel(new ImageIcon(getClass().getResource("csa_splash.png")));
    JLabel splashlabel = new JLabel(new ImageIcon(appBase + File.separator + "Startup" + File.separator + "csa_splash.png"));

    JPanel splashlabelpanel=new JPanel(new FlowLayout(FlowLayout.CENTER));
    JPanel splashpanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    Dimension dimScreen =
      Toolkit.getDefaultToolkit().getScreenSize();
    Dimension labelSize = splashlabel.getPreferredSize();

    JLabel clientUpdatelabel = new JLabel("             FORMS Server Running             ",JLabel.CENTER);
    JPanel clientUpdatelabelpanel=new JPanel(new FlowLayout(FlowLayout.CENTER,15,10));
    JProgressBar progress = new JProgressBar();
    JPanel clientUpdatebuttonpanel=new JPanel();
    BoxLayout box=new BoxLayout(clientUpdatebuttonpanel,BoxLayout.Y_AXIS);
    JPanel clientUpdatepanel = new JPanel(new FlowLayout(FlowLayout.CENTER,1000,40));

    // Variable used for thread
    boolean threadrun=false;

    // Only want one instance of program running.  This variable used to 
    // enforce this.
    static boolean portAvailable=true;

    // enumerate cycles because exception window doesn't display correctly in first cycle
    static int numcycles=0;

    static ServerSocket serverSocket;

    // listening for ecodes?
    static boolean listening;

    // Embedded Tomcat object
    static Embedded eTomcat = new Embedded();

    //////////////////////////////////////////////////////// 
    //  CONSTRUCTOR -- SETS UP AND DISPLAYS INITIAL PANEL //
    ////////////////////////////////////////////////////////

    public FORMSTomcatServer()
    {

        super( "FORMS CSA Tomcat Server Process" );

        // Enable mouse click to cause splash/about screen to disappear
        addMouseListener(new MouseAdapter()
            {
                public void mousePressed(MouseEvent e)
                {
                    hide();
                }
            });

        // Set Icon for main JFrame
        //setIconImage( new ImageIcon(
        //    getClass().getResource( "FORMS.gif" ) ).getImage() );
        setIconImage( new ImageIcon(appBase + File.separator + "Startup" + File.separator + "FORMS.gif"  ).getImage() );


        splashlabelpanel.add(splashlabel);
        splashpanel.add(splashlabelpanel);

        // change this according to the number of buttons used
        getContentPane().setLayout( new FlowLayout(FlowLayout.CENTER,15,5) );

        clientUpdatelabel.setForeground(new Color(150,0,25));
        clientUpdatelabel.setForeground(new Color(0,80,20));
        clientUpdatelabel.setFont(new Font(clientUpdatelabel.getFont().getName(),clientUpdatelabel.getFont().getStyle(),clientUpdatelabel.getFont().getSize()+5));
        clientUpdatelabelpanel.add(clientUpdatelabel);
        clientUpdatelabelpanel.setBackground(new Color(240,240,244));
        clientUpdatelabelpanel.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.RAISED),new EtchedBorder()));
        clientUpdatebuttonpanel.setLayout(box);
        clientUpdatebuttonpanel.add(Box.createVerticalStrut(15));
        clientUpdatebuttonpanel.add(progress);
        clientUpdatebuttonpanel.setBackground(bgColor);
        clientUpdatepanel.add(clientUpdatelabelpanel);
        clientUpdatepanel.add(clientUpdatebuttonpanel);
        clientUpdatepanel.setBackground(bgColor);

        c.setLayout(new BorderLayout());
        c.setBackground(bgColor);
        c.add("Center",splashpanel);
        c.setSize(labelSize);
        this.pack();
        this.setLocation(dimScreen.width/2 - (labelSize.width/2),
                         dimScreen.height/2 - (labelSize.height/2));

        // create the menu
        createMenu();

        // Wait up to 40 seconds to see if we have internet connectivity (pen tablets
        // wireless connections take a while to be established.  If we don't wait
        // for them, we might never check for updates.
        // If not connected, IP address will be returned as 127.0.0.1, otherwise will 
        // have a network IP address.
        for (int i=1; i<=40; i++) {
           String myipaddress;
           try {
              myipaddress=InetAddress.getLocalHost().getHostAddress();
              if (!myipaddress.equals("127.0.0.1")) {
                 System.out.println("Internet Connectivity Response - Connected!!!");
                 break;
              } else if (i==40) {
                 System.out.println("Internet Connectivity Response - Not connected.");
              }
              Thread.sleep(1000); 
           } catch (Exception ex) {}
        }

        listening=true;
        try {
          try {
             serverSocket = new ServerSocket(51767);

             // Show splash screen
             Thread s1=new Thread(new Runnable() {
                public void run() {
                   try {
             show();
             Thread.sleep(1500); 
             hide();
                   } catch (Exception e) {}
                }
             });
             s1.start();

             // Start server running
	     startTomcat();

          } catch( Exception e3 ) {
          
             portAvailable=false;
             System.out.println("NO PORT AVAILABLE!!!!");

             // Show splash log with information dialog (no timeout)
             Thread s1=new Thread(new Runnable() {
                public void run() {
                   try {
             show();
                   } catch (Exception e) {}
                }
             });
             s1.start();
          }
        } catch (Exception e) {}

        if (!portAvailable) {
           java.awt.Toolkit.getDefaultToolkit().beep();
           toFront();
           c.requestFocus();
           JOptionPane.showMessageDialog( this, 
              "ERROR:  Only one instance of this program may be running.        \n\n" +
              "     If you believe you have received this message in error, try\n" +
              "     accessing the program again in several minutes.  If you\n" +
              "     are still unsuccessful, contact a programmer.\n" 
                                        );
           // EXIT PROCESS
           System.exit( 0 );
        }

    }

    /////////////////////////////////////////////////////////////////////////////////////
    // MAIN METHOD -- CALLS CONSTRUCTOR -- WE ARE ATTACHING TO THE SOCKET HERE BECAUSE //
    // IF THIS IS DONE IN THE CONSTRUCTOR METHOD, THE SOCKET IS RELEASED WHEN THE      //
    // WINDOW IS CLOSED.  WE WANT THE SOCKET TO BE HELD AS LONG AS THE PROGRAM IS      //
    // RUNNING.                                                                        //
    /////////////////////////////////////////////////////////////////////////////////////

    public static void main( String[] args )
    {
        try{ UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() ); }
        catch( Exception e ) {}

        new FORMSTomcatServer();
        listening=true;

    }

    public static void runServer() {
       // SET UP THREAD 
       Thread t1=new Thread(new Runnable() {
          public void run() {
             try {
   	       startTomcat();
             } catch (Exception e) {}
          }
       });
       t1.start();
    }

    ////////////////////////////////////////////////////////////////////////
    // OVERRIDDEN menuItemSelected METHOD - RUNS WHEN A SYSTRAY MENU ITEM //
    // IS SELECTED                                                        //
    ////////////////////////////////////////////////////////////////////////

    public void menuItemSelected( SysTrayMenuEvent e )
    {

        if (e.getActionCommand().equals( "start" )) {
            menu.getItem("Start FORMS Tomcat Server").setEnabled(false);
            menu.getItem("Stop FORMS Tomcat Server").setEnabled(false);
            menu.getItem("Restart FORMS Tomcat Server").setEnabled(false);
            listening=true;
            threadrun=true;
            startTomcat();
            try { 
                  Thread.sleep(3000); 
                } catch (Exception e4) {}
            menu.getItem("Stop FORMS Tomcat Server").setEnabled(true);
            menu.getItem("Restart FORMS Tomcat Server").setEnabled(true);
        } else if (e.getActionCommand().equals( "restart" )) {
            menu.getItem("Start FORMS Tomcat Server").setEnabled(false);
            menu.getItem("Stop FORMS Tomcat Server").setEnabled(false);
            menu.getItem("Restart FORMS Tomcat Server").setEnabled(false);
            listening=false;
            threadrun=false;
            stopTomcat(eTomcat);
            try { 
                  Thread.sleep(3000); 
                } catch (Exception e4) {}
            listening=true;
            threadrun=true;
            startTomcat();
            try { 
                  Thread.sleep(3000); 
                } catch (Exception e4) {}
            menu.getItem("Stop FORMS Tomcat Server").setEnabled(true);
            menu.getItem("Restart FORMS Tomcat Server").setEnabled(true);
        } else if (e.getActionCommand().equals( "stop" )) {
            menu.getItem("Start FORMS Tomcat Server").setEnabled(false);
            menu.getItem("Stop FORMS Tomcat Server").setEnabled(false);
            menu.getItem("Restart FORMS Tomcat Server").setEnabled(false);
            listening=false;
            threadrun=false;
            stopTomcat(eTomcat);
            try { 
                  Thread.sleep(3000); 
                } catch (Exception e4) {}
            menu.getItem("Start FORMS Tomcat Server").setEnabled(true);
            menu.getItem("Restart FORMS Tomcat Server").setEnabled(true);
        } else if (e.getActionCommand().equals( "exit" )) {
            // EXIT PROCESS
            java.awt.Toolkit.getDefaultToolkit().beep();
            toFront();
            c.requestFocus();
            int sval=JOptionPane.showConfirmDialog(this,
               "Are you sure you want to exit the FORMS Tomcat Server?     \n" +
               "You will not be able to access the FORMS CSA application.",
               "Confirm Exit",JOptionPane.YES_NO_OPTION);
            if (sval==0) {
               System.exit( 0 );
            }
        } else if (e.getActionCommand().equals( "about" )) {
            // SHOW ABOUT BOX
            //JOptionPane.showMessageDialog( this, "FORMS CSA Tomcat Server" );
            setVisible(true);
            c.repaint();
        } else {
            setVisible(true);
            c.repaint();
        }
    }

    //////////////////////////////////////////////////////////////////////////
    // THE FOLLOWING TWO METHODS RESPOND TO LEFT MOUSE BUTTON CLICKS ON THE //
    // SYSTRAY ICON.  THERE SEEMS TO BE A BUG IN THE SYSTRAY SOFTWARE       //
    // KEEPING THESE FROM WORKING, THOUGH I HAVE FOUND NO DOCUMENTATION ON  //
    // THIS TO VERIFY IT OTHER THAN ONE USEGROUP MESSAGE THAT I THOUGHT     //
    // MAY SUGGEST THAT THIS IS A KNOW PROBLEM.                             //
    //////////////////////////////////////////////////////////////////////////

    public void iconLeftClicked( SysTrayMenuEvent e )
    {
        // NOTE:  The left mouse methods to not seem to work (seems to be
        //        a SysTray bug.  We don't really need them anyway
    }

    public void iconLeftDoubleClicked( SysTrayMenuEvent e )
    {
        // NOTE:  The left mouse methods to not seem to work (seems to be
        //        a SysTray bug.  We don't really need them anyway
        setVisible(true);
        c.repaint();
    }

    /////////////////////////////////////////////////////////////////////////////////
    // OVERRIDDEN createMenu METHOD USED TO CREATE SELECTION MENU FOR SYSTRAY ICON //
    /////////////////////////////////////////////////////////////////////////////////

    void createMenu()
    {
        // create an exit item
        SysTrayMenuItem itemStart = new SysTrayMenuItem( "Start FORMS Tomcat Server", "start" );
        itemStart.addSysTrayMenuListener( this );

        // create an exit item
        SysTrayMenuItem itemRestart = new SysTrayMenuItem( "Restart FORMS Tomcat Server", "restart" );
        itemRestart.addSysTrayMenuListener( this );

        // create an exit item
        SysTrayMenuItem itemStop = new SysTrayMenuItem( "Stop FORMS Tomcat Server", "stop" );
        itemStop.addSysTrayMenuListener( this );

        // create an exit item
        SysTrayMenuItem itemExit = new SysTrayMenuItem( "Exit", "exit" );
        itemExit.addSysTrayMenuListener( this );

        // create an about item
        SysTrayMenuItem itemAbout = new SysTrayMenuItem( "About...", "about" );
        itemAbout.addSysTrayMenuListener( this );

        // create the main menu
        menu = new SysTrayMenu( icons[ 0 ], toolTip );

        // insert items
        menu.addItem( itemExit );
        menu.addSeparator();
        menu.addItem( itemAbout );
        menu.addSeparator();
        menu.addItem( itemStop );
        menu.addItem( itemRestart );
        menu.addItem( itemStart );

        itemStart.setEnabled(false);
        itemRestart.setEnabled(false);
        itemStop.setEnabled(false);
        
    }

    ////////////////////////////////////////////////////////////////////////////////
    // METHOD CALLED BY ACTION AND KEYPRESSED METHODS -- NOT AN OVERRIDDEN METHOD //
    // THIS METHOD CONTAINS THE ACTUAL RESPONSES TO VARIOUS BUTTON PRESSES.       //
    ////////////////////////////////////////////////////////////////////////////////

    public void sourceSelect(Object source) {

       // DOES NOTHING FOR NOW

    }

    ////////////////////////////////////////////////////////////////////////////////
    // KEY PRESSED EVENT METHOD (PERFORM CLICK EVENT (ACTIONEVENT) - DONE TO MAKE //
    // KEY LOOK CLICKED.  THIS IS THE OVERRIDDEN KEYLISTENER METHOD.  WE ARE      //
    // ISSUING A DOCLICK INSTEAD OF A DIRECT RESPONSE SO THE BUTTON LOOKS         //
    // CLICKED.  THERE WAS NO CHANGE TO THE APPEARANCE OF THE BUTTONS WHEN THIS   //
    // METHOD ISSUED A DIRECT RESPONSE (I.E. CALL TO SELECTSOURCE METHOD).        //
    ////////////////////////////////////////////////////////////////////////////////

    public void keyPressed(KeyEvent ke){ 
       Object source = ke.getSource();
    } 
    public void keyTyped(KeyEvent ke){ 
    } 
    public void keyReleased(KeyEvent ke){ 
    } 

    ///////////////////////////////////////////////////////////////////////////
    // ACTION EVENT METHOD -- OVERRIDDEN ACTIONLISTENER METHOD.  RESPONDS TO //
    // KEYCLICKS.                                                            //
    ///////////////////////////////////////////////////////////////////////////

    public void actionPerformed(ActionEvent e) {
       Object source = e.getSource();
       sourceSelect(source);
    }

    public static void startTomcat() {

        //eTomcat.setLogger(null);
        MemoryRealm memRealm = new MemoryRealm();
        eTomcat.setRealm(memRealm);
      
        // create an Engine
        Engine baseEngine = eTomcat.createEngine();
        //baseEngine = eTomcat.createEngine();
        // set Engine properties
        baseEngine.setName("formsEngine");
        baseEngine.setDefaultHost("formsHost");
        
        // create Host
        Host baseHost = eTomcat.createHost("formsHost", "webapps");
     
        // add host to Engine
        baseEngine.addChild(baseHost);

        File rootFile=new File(appBase + File.separator + "Catalina" + File.separator + "webapps" + File.separator + "ROOT");
        if (rootFile.exists()) {
           // create root Context
           Context rootCtx = eTomcat.createContext("", rootFile.getAbsolutePath());
           // add context to host
           baseHost.addChild(rootCtx);
        } 
        File mgrFile=new File(appBase + File.separator + "Catalina" + File.separator + "webapps" + File.separator + "manager");
        if (mgrFile.exists()) {
        
           // create application Context
           Context mgrCtx = eTomcat.createContext("/manager", appBase + File.separator + "catalina" + File.separator + "webapps" + File.separator + "manager");
           mgrCtx.setPrivileged(true);
           // add context to host
           baseHost.addChild(mgrCtx);
        } 
        // FORMS CSA Development Server
        if (new File(appBase + File.separator + "Dev").exists()) {
           Context devCtx = eTomcat.createContext("/FORMSClientDev",appBase + File.separator + "Dev");
           baseHost.addChild(devCtx);
        }
        // FORMS CSA Test Server
        if (new File(appBase + File.separator + "Test").exists()) {
           Context testCtx = eTomcat.createContext("/FORMSClientTest",appBase + File.separator + "Test");
           baseHost.addChild(testCtx);
        }
        // FORMS CSA Production Server
        if (new File(appBase + File.separator + "Prod").exists()) {
           Context prodCtx = eTomcat.createContext("/FORMSClient",appBase + File.separator + "Prod");
           baseHost.addChild(prodCtx);
        }
            
        // add new Engine to set of Engine for embedded server
        eTomcat.addEngine(baseEngine);        
  
        int httpPort;
        int httpsPort;
        HashMap m=FORMSStartupConfig.readConfig(appBase + File.separator + "Startup" + File.separator + "Startup.conf");
        try {
           httpPort=new Integer((String)m.get("httpport")).intValue();
           httpsPort=new Integer((String)m.get("httpsport")).intValue();
        } catch (Exception e) {
           httpPort=8080;
           httpsPort=8443;
        }
        startTomcat(eTomcat,baseEngine,httpPort,httpsPort);
    }

    private static int startTomcat(Embedded eTomcat,Engine baseEngine,
                                   int httpPort, int httpsPort) {

        // create Connector 
        Connector httpConnector = eTomcat.createConnector((java.net.InetAddress) null,
                            httpPort, false);
        Connector httpsConnector = eTomcat.createConnector((java.net.InetAddress) null,
                            httpsPort, true);
        // add new Connector to set of Connectors for embedded server, associated
        // with Engine
        HashMap m=FORMSStartupConfig.readConfig(appBase + File.separator + "Startup" + File.separator + "Startup.conf");
        IntrospectionUtils.setProperty(httpsConnector,"sslProtocol","TLS");
        IntrospectionUtils.setProperty(httpsConnector,"keystore",appBase + File.separator + ((String)m.get("keystore")).trim());
        IntrospectionUtils.setProperty(httpsConnector,"keypass",((String)m.get("keypass")).trim());
        IntrospectionUtils.setProperty(httpsConnector,"scheme","https");
        IntrospectionUtils.setProperty(httpsConnector,"secure","true");
        IntrospectionUtils.setProperty(httpsConnector,"clientAuth","false");

        httpConnector.setRedirectPort(httpsPort);

        eTomcat.addConnector(httpConnector);
        eTomcat.addConnector(httpsConnector);
        //httpsConnector.setProtocol("TLS");

        // Add host restriction valve, per setup file
        String allowlist=(String)m.get("hostallow");
        if (allowlist!=null && allowlist.trim().length()>0) {
           RemoteAddrValve restrict=new RemoteAddrValve();
           restrict.setAllow(allowlist);
           Pipeline p=baseEngine.getPipeline();
           p.addValve(restrict);
        }
        // 
        // This valve currently doesn't work under Tomcat 6
        // 
        //BadInputValve inputFilter=new BadInputValve();
        //inputFilter.setDeny("\\x00,\\x04,\\x08,\\x0a,\\x0d");
        //p.addValve(inputFilter);

        // This may be required, else an exception may be thrown on startup.
        // This seems to depend on exact classpath ordering, however.
        eTomcat.setCatalinaHome(appBase + File.separator + "Catalina");
        
        // start server
        try {
            eTomcat.start();
            return 1;
        } catch (Exception ex) {
            if ((ex.getMessage().toLowerCase().indexOf("bindexception"))>=0) {
               return appRunning(httpPort);

            } else {
               return -1;
            }
        } 

    }

    private static int stopTomcat(Embedded eTomcat) {
        // stop server
        try {
            eTomcat.stop();
            // The following make the stop/restart process more reliable
            // remove valves
            org.apache.catalina.Container c=eTomcat.getContainer();
            Pipeline p=c.getPipeline();
            Valve[] v=p.getValves();
            for (int i=0;i<v.length;i++) {
               p.removeValve(v[i]);
            }
            // remove listeners
            LifecycleListener[] tl=eTomcat.findLifecycleListeners();
            for (int i=0;i<tl.length;i++) {
               eTomcat.removeLifecycleListener(tl[i]);
            }
            // remove connectors
            Connector[] tc=eTomcat.findConnectors();
            for (int i=0;i<tc.length;i++) {
               eTomcat.removeConnector(tc[i]);
            }
            // remove embedded object
            eTomcat=null;

            return 1;
        } catch (Exception ex) {
            //ex.printStackTrace(System.out);
            return 0;
        } 

    }

    private static int appRunning(int httpPort) {
       try {
          URL url=null;
          URLConnection c=null;
          // site will return 302 header if redirected to HTTPS and 200 if successful,
          // otherwise site is not up and should start new tomcat
          url=new URL("http://localhost:" + httpPort + "/FORMSClient/login.do");
          c=url.openConnection();
          c.connect();
          if (c.getHeaderField(0).matches("(.* 302 .*)|(.* 200 .*)")) {
             return 9;
          } 
          url=new URL("http://localhost:" + httpPort + "/FORMSClientTest/login.do");
          c=url.openConnection();
          c.connect();
          if (c.getHeaderField(0).matches("(.* 302 .*)|(.* 200 .*)")) {
             return 9;
          } 
          url=new URL("http://localhost:" + httpPort + "/FORMSClientDev/login.do");
          c=url.openConnection();
          c.connect();
          if (c.getHeaderField(0).matches("(.* 302 .*)|(.* 200 .*)")) {
             return 9;
          } 
          return 0;
       } catch (Exception ex) {
          //ex.printStackTrace(System.out);
          return -1;
       }
    }

}

