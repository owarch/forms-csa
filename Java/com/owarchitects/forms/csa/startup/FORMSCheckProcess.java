
///////////////////////////////////////////////////////////////////////////////////
//                                                                               //
// PROGRAM:  FORMSCheckProcess.java                                              //     
//                                                                               //
// PURPOSE:  This program provides an interface to start/stop/restart the FORMS  //
//    Tomcat Server.                                                             //
//                                                                               //
///////////////////////////////////////////////////////////////////////////////////

package com.owarchitects.forms.csa.startup;

import java.io.*;
import java.net.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;
import HTTPClient.*;
import HTTPClient.CookieModule;
import org.apache.commons.lang.StringUtils;

public class FORMSCheckProcess extends JFrame implements ActionListener, KeyListener 
{

    java.awt.Container c = getContentPane();
 
    //Color bgColor=new Color(215,215,255);
    Color bgColor=new Color(24,51,102);
 
    JLabel mainlabel = new JLabel("             Select Client-Side Application to Start             ",JLabel.CENTER);
    JPanel mainlabelpanel=new JPanel(new FlowLayout(FlowLayout.CENTER,15,10));
    JButton prodbutton = new JButton("     FORMS Client-Side Production Server     ");
    JButton testbutton = new JButton("     FORMS Client-Side Test Server     ");
    JButton devbutton = new JButton("     FORMS Client-Side Development Server     ");
    JButton catbutton = new JButton("     Start Tomcat Server Only     ");
    JPanel mainbuttonpanel=new JPanel(new GridLayout(6,1,5,5));
    JPanel mainpanel = new JPanel(new FlowLayout(FlowLayout.CENTER,1000,40));
    JLabel waitlabel = new JLabel("             Waiting for Tomcat Server to Initialize....             ",JLabel.CENTER);
    JPanel waitlabelpanel=new JPanel(new FlowLayout(FlowLayout.CENTER,15,10));
    JPanel waitpanel = new JPanel(new FlowLayout(FlowLayout.CENTER,1000,40));

    int httpPort;
    int httpsPort;

    static String javahome=System.getProperty("java.home");
    static String appBase=javahome.substring(0,javahome.lastIndexOf(File.separator));

    public static void main( String[] args ) {
       new FORMSCheckProcess(args);
    }

    FORMSCheckProcess(String[] args) {

      super( "FORMS Check Process" );

      HashMap m=readConfig();
      try {
         httpPort=new Integer((String)m.get("httpport")).intValue();
         httpsPort=new Integer((String)m.get("httpsport")).intValue();
      } catch (Exception e) {
         httpPort=8080;
         httpPort=8443;
      }

      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

      // See if port is available     
      boolean already=true;     
      try {
         ServerSocket serverSocket = new ServerSocket(51767);
         already=false;
         // Don't keep socket open in this program
         serverSocket.close();
      } catch( Exception e3 ) { }

      // Set Icon for main JFrame
      //setIconImage( new ImageIcon(
      //    getClass().getResource( "FORMS.gif" ) ).getImage() );
      setIconImage( new ImageIcon( appBase + File.separator + "Startup" + File.separator + "FORMS.gif" ).getImage() );


      // if server is running, make sure tomcat is available, don't start
      // job until it is.
      waitlabel.setForeground(new Color(150,0,25));
      waitlabel.setForeground(new Color(0,80,20));
      waitlabel.setFont(new Font(waitlabel.getFont().getName(),waitlabel.getFont().getStyle(),waitlabel.getFont().getSize()+5));
      waitlabelpanel.add(waitlabel);
      waitlabelpanel.setBackground(new Color(224,224,224));
      waitlabelpanel.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.RAISED),new EtchedBorder()));
      waitpanel.add(waitlabelpanel);
      waitpanel.setBackground(bgColor);

      c.setLayout(new BorderLayout());
      c.setBackground(bgColor);
      c.add("Center",waitpanel);

      Dimension dimScreen = Toolkit.getDefaultToolkit().getScreenSize();
      Dimension panelSize = new Dimension(600,160);
      this.setLocation(dimScreen.width/2 - (panelSize.width/2),
                       dimScreen.height/2 - (panelSize.height/2));
      setSize(panelSize);

      if (already) {
         for (int i=1; i<=100; i++) {
            try {
               CookieModule.setCookiePolicyHandler(null);
               HTTPConnection c = new HTTPConnection("localhost",httpPort);
               c.setTimeout(1000);
               HTTPResponse r = c.Get("/");
               int response=r.getStatusCode();
               setVisible(false);
               break;
            } catch (java.io.InterruptedIOException e1) {
               setVisible(true);
               try { Thread.sleep(1000); } catch (Exception e2) { }
            } catch (Exception e1) {
               setVisible(true);
               try { Thread.sleep(1000); } catch (Exception e2) { }
            }
         }
      }

      // Check which applications are available 
      String path=getClass().getResource("FORMSCheckProcess.class").toString();
      path=path.replaceFirst("[/\\\\][^/\\\\]*[/\\\\][Ww][Ee][Bb][-][Ii][Nn][Ff].*$","");
      path=path.replaceFirst("^[Ff][Ii][Ll][Ee][:]*[/\\\\]*","");
      path=path.replaceAll("%20"," ");
      boolean devforms=false;
      boolean testforms=false;
      boolean prodforms=false;
      File tempfile;
      tempfile=new File(path + "/" + "Dev");
      if (tempfile.exists()) devforms=true;
      tempfile=new File(path + "/" + "Test");
      if (tempfile.exists()) testforms=true;
      tempfile=new File(path + "/" + "Prod");
      if (tempfile.exists()) prodforms=true;

      prodbutton.addActionListener(this);
      prodbutton.addKeyListener(this);
      testbutton.addActionListener(this);
      testbutton.addKeyListener(this);
      devbutton.addActionListener(this);
      devbutton.addKeyListener(this);
      catbutton.addActionListener(this);
      catbutton.addKeyListener(this);

      mainlabel.setForeground(new Color(150,0,25));
      //mainlabel.setForeground(new Color(0,100,25));
      mainlabel.setForeground(new Color(0,80,20));
      mainlabel.setFont(new Font(mainlabel.getFont().getName(),mainlabel.getFont().getStyle(),mainlabel.getFont().getSize()+5));
      mainlabelpanel.add(mainlabel);
      //mainlabelpanel.setBackground(new Color(240,240,244));
      mainlabelpanel.setBackground(new Color(224,224,224));
      mainlabelpanel.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.RAISED),new EtchedBorder()));
      if (prodforms) mainbuttonpanel.add(prodbutton);
      if (testforms) mainbuttonpanel.add(testbutton);
      if (devforms) mainbuttonpanel.add(devbutton);
      if (!already) mainbuttonpanel.add(catbutton);

      int prodnum=(prodforms) ? 1 : 0;
      int testnum=(testforms) ? 1 : 0;
      int devnum=(devforms) ? 1 : 0;
      int alreadynum=(already) ? 1 : 0;

      mainbuttonpanel.setBackground(bgColor);
      mainpanel.add(mainlabelpanel);
      mainpanel.add(mainbuttonpanel);
      mainpanel.setBackground(bgColor);

      c.setLayout(new BorderLayout());
      c.setBackground(bgColor);
      c.remove(waitpanel);
      c.add("Center",mainpanel);

      panelSize = new Dimension(520,320);
      this.setLocation(dimScreen.width/2 - (panelSize.width/2),
                       dimScreen.height/2 - (panelSize.height/2));
      setSize(panelSize);

      System.out.println(already);

      boolean alreadyvar=false;
      for (int i=0;i<args.length;i++) {
         if (args[i].equalsIgnoreCase("dev")) {
            if (devforms) {
               System.out.println("csadev");
               System.exit(0);
               return;
            } else break;
         } else if (args[i].equalsIgnoreCase("test")) {
            if (testforms) {
               System.out.println("csatest");
               System.exit(0);
               return;
            } else break;
         } else if (args[i].equalsIgnoreCase("prod")) {
            if (prodforms) {
               System.out.println("csaprod");
               System.exit(0);
               return;
            } else break;
         } else if (args[i].equalsIgnoreCase("tomcat")) {
               return;
         }
      }

      if ((prodnum + testnum + devnum)>1) {
      
         setVisible(true);
         c.repaint();
      
      } else if (prodforms) {
         System.out.println(httpPort);
         System.out.println("csaprod");
         System.exit(0);
      } else if (testforms) {
         System.out.println(httpPort);
         System.out.println("csatest");
         System.exit(0);
      } else if (devforms) {
         System.out.println(httpPort);
         System.out.println("csadev");
         System.exit(0);
      } else if (devforms) {
         System.exit(0);
      }

    }

    ///////////////////////////////////////////////////////////////////////////
    // ACTION EVENT METHOD -- OVERRIDDEN ACTIONLISTENER METHOD.  RESPONDS TO //
    // KEYCLICKS.  NOTICE THE WORK IS DONE BY THE sourceSelect METHOD.       //
    ///////////////////////////////////////////////////////////////////////////

    public void actionPerformed(ActionEvent e) {
       Object source = e.getSource();
       sourceSelect(source);
    }

    public void sourceSelect(Object source) {

       if (source==prodbutton) {
       
          System.out.println(httpPort);
          System.out.println("csaprod");
          System.exit(0);

       } else if (source==testbutton) {
       
          System.out.println(httpPort);
          System.out.println("csatest");
          System.exit(0);

       } else if (source==devbutton) {
       
          System.out.println(httpPort);
          System.out.println("csadev");
          System.exit(0);

       } else if (source==catbutton) {
       
          System.exit(0);

       }
 
    }

    ////////////////////////////////////////////////////////////////////////////////
    // KEY PRESSED EVENT METHOD (PERFORM CLICK EVENT (ACTIONEVENT) - DONE TO MAKE //
    // KEY LOOK CLICKED.  THIS IS THE OVERRIDDEN KEYLISTENER METHOD.  WE ARE      //
    // ISSUING A DOCLICK INSTEAD OF A DIRECT RESPONSE SO THE BUTTON LOOKS         //
    // CLICKED.  THERE WAS NO CHANGE TO THE APPEARANCE OF THE BUTTONS WHEN THIS   //
    // METHOD ISSUED A DIRECT RESPONSE (I.E. CALL TO SELECTSOURCE METHOD).        //
    ////////////////////////////////////////////////////////////////////////////////

    public void keyPressed(KeyEvent ke){ 
       Object source = ke.getSource();
       // ONLY IF ENTER KEY IS PRESSED
       if (ke.getKeyCode()==KeyEvent.VK_ENTER) {
          if (source==prodbutton) {
             prodbutton.doClick();
          } else if (source==testbutton) {
             testbutton.doClick();
          } else if (source==devbutton) {
             devbutton.doClick();
          } else if (source==catbutton) {
             catbutton.doClick();
          }
       }
    } 
    public void keyTyped(KeyEvent ke){ 
    } 
    public void keyReleased(KeyEvent ke){ 
    } 

    // Read Startup.conf file
    private static HashMap readConfig() {
       try {
          HashMap rm=new HashMap();
          String f = appBase + File.separator + "Startup" + 
                     File.separator + "Startup.conf";
          BufferedReader in = new BufferedReader(new FileReader(f));
          String line;
          while((line = in.readLine()) != null) {
             if (StringUtils.contains(line.toLowerCase(),"httpport:")) {
                rm.put("httpport",StringUtils.trim(StringUtils.substringAfter(line,"httpport:")));
             } else if (StringUtils.contains(line.toLowerCase(),"httpsport:")) {
                rm.put("httpsport",StringUtils.trim(StringUtils.substringAfter(line,"httpsport:")));
             }
             
          }
          return rm;
       } catch (Exception e) {   
          return null;
       } 
    }


}


