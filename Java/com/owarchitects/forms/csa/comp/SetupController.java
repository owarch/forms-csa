 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/* 
 *
 * SetupController.java -  CSA Database Setup Program
 *
 *   This class extends FORMSServiceClientController rather than FORMSCsaServiceClientController because it
 *   is a non-standard CSA controller in the sense that it is called outside a CSA
 *   logged in session
 *
 */

package com.owarchitects.forms.csa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.io.*;
import java.lang.*;
import java.security.*;
import javax.crypto.SecretKey;
import javax.servlet.http.*;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.*;
import org.springframework.web.context.support.*;
import org.springframework.context.support.AbstractApplicationContext;
import org.apache.axis2.context.*;
import org.apache.axis2.client.*;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.commons.io.FileUtils;
//import org.apache.commons.dbcp.BasicDataSource;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.orm.hibernate3.HibernateJdbcException;
import org.apache.tools.ant.*;
import org.springframework.orm.hibernate3.LocalSessionFactoryBean;
import org.hibernate.*;
import org.hibernate.metadata.*;
import org.springframework.beans.factory.FactoryBean; 
import java.lang.reflect.*;
import org.hibernate.persister.entity.*;

public class SetupController extends FORMSServiceClientController {

   private String url;
   private String user;
   private String passwd;
   private boolean isRunning=false;

   // Override superclass handleRequestInternal method
   protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws FORMSException {

        try {

           // PrintWriter for diagnostic output
           out=response.getWriter();        

           this.request=request;
           this.response=response;
           this.syswork=null;
           this.sysconn=null;
           // Pull syswork, sysconn from session attribute preferably, alternately from cookie.
           try {
              this.syswork=request.getSession().getAttribute("syswork").toString();
           } catch (Exception swe) {}
           if (syswork==null || syswork.length()<1) {
              this.syswork=Base64.decode(this.getCookieValue(Base64.encode("workdir")).replaceAll("=",""));
           }   
           response.addCookie(new Cookie(Base64.encode("workdir"),Base64.encode(syswork)));
           try {
              this.sysconn=request.getSession().getAttribute("sysconn").toString();
           } catch (Exception swe) {}
           if (sysconn==null || sysconn.length()<1) {
              this.sysconn=Base64.decode(this.getCookieValue(Base64.encode("sysconn")).replaceAll("=",""));
           }   
           // formstime used for sysconn and auth
           String formstime=new Long(System.currentTimeMillis()).toString();

           //// Ignore sysconn for setup program, no need to persist this across sessions
           //if (sysconn==null || sysconn.length()<1) {
           //   safeRedirect("login.do");
           //}
           if (sysconn==null || sysconn.length()<1) {
              Sysconn sconn=new Sysconn();
              sconn.setUsername(request.getParameter("user"));
              sconn.setPassword(request.getParameter("passwd"));
              sconn.setFormstime(formstime);
              if (sconn.getUsername()!=null) {
                 try {
                    sysconn=EncryptUtil.encryptString(ObjectXmlUtil.objectToXmlString(sconn),
                                        SessionObjectUtil.getInternalKeyObj(session));
                    request.getSession().setAttribute("sysconn",sysconn);
                    this.refreshContext();
                 } catch (FORMSException fex) {
                    // ok, keystore may not yet be set up
                 }
              } 
           }


           // Get context
           session=request.getSession();
           wp=SessionObjectUtil.getWinPropsObj(request,response);
           wp.setWinNameMissOk("Y");
   
           // Immediately expire headers
           response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
           response.setHeader("Pragma","no-cache"); //HTTP 1.0
           response.setDateHeader ("Expires", -1); //prevents caching at the proxy server

           try {

              // Create mav used by override method
              this.mav=new ModelAndView();
              mav=submitRequest();

              if (mav!=null) {
                 // add WinProps objects to view
                 wp.addViewObjects(mav);
                 // add ServletPath (spath) object to view
                 mav.addObject("spath",request.getServletPath().substring(1));
              }    
              return mav;

           } catch (Exception ke) {
                 
              throw new FORMSException("FORMSController exception (submitRequest exception) - <BR>",ke);
           }
          
        } catch (Exception e) {

           throw new FORMSException("FORMSController exception - <BR>",e);

        }

   }     

   public ModelAndView submitRequest() throws FORMSException {

      if (request.getParameter("url")!=null) {

         user=request.getParameter("user");
         passwd=request.getParameter("passwd");
         url=request.getParameter("url");
         
         if (!isRunning) {

            // Create random DBPW for Main database.  Read MainDB info into context
            String dbpw=PasswordUtil.getRandomPassword();
            SessionObjectUtil.refreshApplicationContext(getSession(),dbpw);

            // Run database creation thread
            SetupThread proc=new SetupThread(getContext(),getMainDAO(),dbpw);
            proc.start();
         }   

         mav.addObject("status","THREAD");
         return mav;

      } else {

         mav.addObject("status","DEFAULT");
         return mav;

      }


    }


    // SetupThread - Performs actual setup as a thread

    private class SetupThread extends Thread {

       com.owarchitects.forms.commons.db.MainDAO maindao;
       AbstractApplicationContext context;

       IframeReloaderResult result;
       StringBuilder sb;
       String dbpassword;

       public SetupThread(AbstractApplicationContext context,com.owarchitects.forms.commons.db.MainDAO maindao,String dbpassword) {
          this.context=context;
          this.maindao=maindao;
          this.dbpassword=dbpassword;
       }

       public void run() {

          if (isRunning) return;

          isRunning=true;
          sb=new StringBuilder();
          sb.append("\n<span style='color:#00AA00'><br>Begin processing:<br><br></span>\n");
          result=new IframeReloaderResult();
          // Only need to set this once, since iframeresult will hold reference to object
          // rather than copy of it
          getSession().setAttribute("iframeresult",result);

          try {

             result.setStatus("RELOAD");

             sbWrite("Checking connectivity and validating supplied credentials: \n");

             CsaServiceSystemParms parms=new CsaServiceSystemParms();
             parms.setUsername(user);
             parms.setPassword(passwd);
             parms.setBeanname("csaVerificationServiceTarget");
             parms.setMethodname("verify");
             
             CsaLoginResult lr=null;

             // Return if problems connecting with SSA
             try {

                lr=(CsaLoginResult)submitServiceRequest(parms,url + "/formsserviceinterceptor.do");

             } catch (Exception e2) {

                sbWrite("<span style='color:#AA0000'><br><br>ERROR!!! Process threw an exception.  " +
                        "Please check your username and password as well as the URL and internet connection.<br><br>" + 
                        Stack2string.getString(e2) + "</span>");
                stopNow(false);        
                return;

             }

             // Return if login failed
             int lstatus=lr.getLoginstatus();
             if (lstatus!=AuthenticatorConstants.LOGGED_IN) {

                sb.append("<span style='color:#AA0000'><br><br>Authentication Error:  ");

                if (lstatus==AuthenticatorConstants.EXCESSIVE_LOGINS) {
                   sb.append("SSA Account has been disabled due to excessive logins.  " +
                             "Please contact FORMS Administrator.");
                } else if (lstatus==AuthenticatorConstants.ACCOUNT_LOCKED) {
                   sb.append("SSA Account has been disabled.  Please contact FORMS Administrator.");
                } else if (lstatus==AuthenticatorConstants.ACCT_EXPIRED) {
                   sb.append("SSA Account has expired.  Please contact FORMS Administrator.");
                } else if (lstatus==AuthenticatorConstants.FAILED) {
                   sb.append("Invalid login credentials.  Please try again.");
                } else if (lstatus==AuthenticatorConstants.HOST_DENIED) {
                   sb.append("SSA access has been disabled for this computer due to excessive " +
                             "invalid login attempts.  Please contact FORMS Administrator.");
                } else {
                   sb.append("CODE " + new AuthenticatorConstants().getFieldName(lstatus));
                }
                sbWrite("<br><br></span>");
                stopNow(false);
                return;

             }

             String pathVar=session.getServletContext().getRealPath("/");

             sbWrite(".....DONE<br><br>\n");

             sbWrite("Create CSA encryption keys: \n");

             ////////////////////////////
             // Create encryption keys //
             ////////////////////////////

             FORMSConfig config=SessionObjectUtil.getConfigObj(getSession());

             // generate FORMSKey 
             
             String fkpassword=PasswordUtil.getRandomPassword();

             String ksfile=pathVar + File.separator + config.getKeyStore();
             String kspassword=config.getKeyStorePassword();              

             // Back up existing keystore file if it exists 
             File ksf=new File(ksfile);
             if (ksf.exists()) {
                ksf.renameTo(new File(ksf.getParentFile(),ksf.getName().replaceFirst("\\.",".bak" +
                   new Long(System.currentTimeMillis()).toString() + ".")));
             }

             // create new keystore file
             KeyStore ks=null;
             ks=java.security.KeyStore.getInstance("JCEKS");
             ks.load(null,kspassword.toCharArray());
             FileOutputStream f = new FileOutputStream(ksfile);
             ks.store(f,kspassword.toCharArray());

             // Create keystore & FORMSKey
             SecretKey formskey=null;
             try {
                formskey=EncryptUtil.createSecretKey(getSession(),kspassword,"formskey",fkpassword);
             } catch(Exception e) {
                sb.append(Stack2string.getString(e) + "<br><br>");
                sbWrite("ERROR:  Could not create FORMSKey<br><br>");
                stopNow(false);
                return;
             }
       
             // Create forms internal key
             SecretKey formsinternal=null;
             try {
                formsinternal=EncryptUtil.createSecretKey(getSession(),kspassword,"formsinternal",kspassword);
             } catch(Exception e) {
                sb.append(Stack2string.getString(e) + "<br><br>");
                sbWrite("ERROR:  Could not create FORMSInternal key<br><br>");
                stopNow(false);
                return;
             }

             // Create user account keypair
             try {
                EncryptUtil.createKeyPair(session,user,passwd);
             } catch (Exception e) {
                sb.append(Stack2string.getString(e) + "<br><br>");
                sbWrite("ERROR:  Could not create user account keypair<br><br>");
                stopNow(false);
                return;
             }

             sbWrite(".....DONE<br><br>\n");

             // Start ant build process 
             sbWrite("Building CSA embedded database (this may take several minutes):  \n");

             ComboPooledDataSource eds=(ComboPooledDataSource)context.getBean("embeddedDataSource");
             eds.setJdbcUrl(eds.getJdbcUrl().replace("$appPath",pathVar));
             eds.setJdbcUrl(eds.getJdbcUrl().replace("create=false","create=true"));

             LocalSessionFactoryBean elsb=(LocalSessionFactoryBean)context.getBean("&sessionFactory");

             elsb.createDatabaseSchema();

             sbWrite(".....DONE<br><br>\n");

             sbWrite("Creating user record in Embeddedusers table\n");
             EmbeddedusersDAO embeddedusersDAO=(EmbeddedusersDAO)context.getBean("embeddedusersDAO");
             Embeddedusers eu=new Embeddedusers();
             eu.setUsername(user);
             eu.setPassword(PasswordUtil.pwEncrypt(passwd));
             eu.setIsdisabled(false);
             eu.setBadlogincount(0);
             eu.setFkpassword(EncryptUtil.encryptString(fkpassword,EncryptUtil.getPublicKey(getSession(),user,passwd)));
             eu.setDbpassword(EncryptUtil.encryptString(dbpassword,EncryptUtil.getPublicKey(getSession(),user,passwd)));
             embeddedusersDAO.saveOrUpdate(eu);

             sbWrite(".....DONE<br><br>\n");

             sbWrite("Building CSA main database (this may take several minutes):  \n");

             // Update database connection strings
             ComboPooledDataSource mds=(ComboPooledDataSource)context.getBean("mainDataSource");
             mds.setJdbcUrl(mds.getJdbcUrl().replace("$appPath",pathVar));
             mds.setJdbcUrl(mds.getJdbcUrl().replace("create=false","create=true"));
             mds.setJdbcUrl(mds.getJdbcUrl().replace("bootPassword=[^<;][^<;]*","bootPassword=" + dbpassword));
             mds.setPassword(dbpassword);
             mds.setOverrideDefaultPassword(dbpassword);

             refreshContext();

             LocalSessionFactoryBean mlsb=(LocalSessionFactoryBean)context.getBean("&mainSessionFactory");

             mlsb.createDatabaseSchema();

             sbWrite(".....DONE<br><br>\n");

             sbWrite("Registering CSA with remote SSA\n");

             CsaRegistrationParms crp=new CsaRegistrationParms();
             crp.setFkpassword(EncryptUtil.encryptString(fkpassword,formsinternal));
             crp.setDbpassword(EncryptUtil.encryptString(dbpassword,formsinternal));
             crp.setUsername(user);
             crp.setPassword(passwd);
             crp.setBeanname("csaRegistrationServiceTarget");
             crp.setMethodname("register");
             
             CsaRegistrationResult crr=null;

             crr=(CsaRegistrationResult)submitServiceRequest(crp,url + "/formsserviceinterceptor.do");

             SsainfoDAO ssainfoDAO=(SsainfoDAO)context.getBean("ssainfoDAO");

             Ssainfo ssai=new Ssainfo();
             ssai.setCsaid(crr.getCsaid());
             ssai.setSsaurl(url);
             ssainfoDAO.saveOrUpdate(ssai);

             sbWrite(".....DONE<br><br>\n");

             // Begin populating Main CSA database
             sbWrite("Populating CSA database (this may take several minutes): <br><br>\n");

             List l;
             Iterator i;

             // pull list of MainDB mapping classes
             SessionFactory sessionFactory = maindao.getSessionFactory();
             Map metadata = sessionFactory.getAllClassMetadata();

             ArrayList persistlist=new ArrayList();
             for (i = metadata.values().iterator(); i.hasNext(); ) {
                 persistlist.add(new ComparablePersister((EntityPersister)i.next(),sessionFactory));
             }    

             Collections.sort(persistlist);

             // iterate through mapping classes and persist remote data to local table
             ArrayList notyet=new ArrayList();
             for (i = persistlist.iterator(); i.hasNext(); ) {
                 String mappingclassname="";
                 ComparablePersister cclass=(ComparablePersister)i.next();
                 mappingclassname=cclass.getName();

                 // Only use transfer classes
                 if (mappingclassname.indexOf("_transfer")<=0) {
                    continue;
                 }
                 // skip persisting certain classes
                 if (mappingclassname.indexOf("Systemlog")>=0 || mappingclassname.indexOf("Tablemodtime")>=0) {
                    continue;
                 }
                 // persist remote table records to local csa database
                 sbWrite("&nbsp;&nbsp;&nbsp;" + mappingclassname +"\n");
                 List rlist=getRemoteTable(mappingclassname);
                 Iterator riter=rlist.iterator();
                 boolean unmetdep=false;
                 while (riter.hasNext()) {
                    Object saveobj=(Object)riter.next();
                    try {
                       maindao.saveOrUpdate(saveobj);
                    } catch (Exception e) {
                       unmetdep=true;
                       notyet.add(saveobj);
                    }
                 }
                 if (unmetdep) {
                    sbWrite("&nbsp;&nbsp;&nbsp;&nbsp;NOTE:  Table has unmet dependencies to be resolved later." +
                               "&nbsp;&nbsp;&nbsp;&nbsp;\n");
                 }
                 sbWrite(".....DONE<br><br>\n");
             }
             sbWrite("Persisting objects with previously unmet dependencies.\n");
             int ivar=0;
             while (notyet.size()>0) {
                sbWrite("<br>&nbsp;&nbsp;&nbsp;Remaining Objects:  " + notyet.size());
                ivar++;
                Iterator niter=notyet.iterator();
                int prevcount=notyet.size();
                while (niter.hasNext()) {
                    Object saveobj=(Object)niter.next();
                    try {

                       maindao.saveOrUpdate(saveobj);
                       niter.remove();

                    } catch (Exception e) {
                       // do nothing
                    }
                }
                if (notyet.size()==prevcount) {

                   niter=notyet.iterator();
                   while (niter.hasNext()) {
                       Object saveobj=(Object)niter.next();
                       Thread.sleep(1);
                   }

                   throw new FORMSException("Could not resolve database dependencies.  Database could not be completely copied");
                };
             } 

             sbWrite(".....DONE<br><br>\n");
             sbWrite("\nPopulation of CSA database is complete<br><br>\n");
             
          } catch (Exception e) {

             sbWrite("<span style='color:#AA0000'><br><br>Warning!!! Process threw an exception. &nbsp;" +
                       "Please check your username and password as well as the URL and internet " +
                       "connection.<br><br><span style='color:#000000'>" +
                       Stack2string.getString(e) + "</span><br><br>" +
                       "<br><br></span>\n");
             stopNow(false);
             return;

          }

          stopNow(true);
          return;

       }

       private List getRemoteTable(String tname) throws FORMSException {

          CsaDataTransferParms cdtp=new CsaDataTransferParms();
          cdtp.setUsername(user);
          cdtp.setPassword(passwd);
          cdtp.setBeanname("csaDataTransferServiceTarget");
          cdtp.setMethodname("getTable");
          cdtp.setMappingClassName(tname);
          
          CsaDataTransferResult cdtr=null;
 
          cdtr=(CsaDataTransferResult)submitServiceRequest(cdtp,url + "/formsserviceinterceptor.do");

          return cdtr.getResultlist();

       }         

       private void stopNow(boolean isok) {
          if (isok) {
             sb.append("<span style='color:#00AA00'>CSA setup completed successfully!<br><br></span>\n");
          } else {
             sb.append("<span style='color:#AA0000'>Setup process was unsuccessful<br><br></span>\n");
          }
          // Make ok button visible
          sb.append("<script>\n");
          sb.append("   var b=parent.document.getElementById('okbutn');\n");
          sb.append("   b.style.visibility='visible';\n");
          if (isok) {
             sb.append("   b.onclick = function() { parent.location.replace(\"login.do\"); }\n");
          } else {
             sb.append("   b.onclick = function() { parent.location.replace(\"setup.do\"); }\n");
          }
          sbWrite("</script>\n");

          result.setStatus("RELOAD");
          result.setContent(sb.toString());
          safeSleep(500);
   
          result.setStatus("DONE");

          isRunning=false;
       }

       private void sbWrite(String ins) {
          sb.append(ins);
          result.setContent(sb.toString());
       }

       private void safeSleep(int ms) {
          try {
             Thread.sleep(ms);
          } catch (Exception e) {
             // Do nothing
          }
       }

    }

}


