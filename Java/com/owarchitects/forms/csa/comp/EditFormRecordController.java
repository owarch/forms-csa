 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * EditFormRecordController.java - Enter New Form Record
 *
 */

package com.owarchitects.forms.csa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
//import java.sql.Clob;
import javax.servlet.http.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
//import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
//import org.springframework.transaction.support.TransactionSynchronizationManager;
//import org.mla.html.table.*;
import org.json.simple.JSONObject;

public class EditFormRecordController extends FORMSCsaServiceClientController {

   private int keycount;
   private final int SELF=0;
   private final int SOMEONE_ELSE=1;
   private HashMap namemap;


   public ModelAndView submitRequest() throws FORMSException {

      // User class-name based view
      mav=new ModelAndView("EditFormRecord");

      try {

         FORMSAuth auth=this.getAuth();

         String dtdefid=getRequest().getParameter("dtdefid");
         String formtypeid=getRequest().getParameter("formtypeid");
         String function=getRequest().getParameter("_function_");

         if (getRequest().getServletPath().indexOf("showformrecords")>=0) {

            try {

            // Create Options Menu Map
            HashMap optionsmap=new HashMap();
            optionsmap.put("holdopt",auth.getOption("holdopt"));
            optionsmap.put("auditopt",auth.getOption("auditopt"));
            auth.setSessionAttribute("optionsobject",optionsmap);

            mav.addObject("status","MATCHRECORDS");   
            ListResult result=new ListResult();
            result.setList(getMatchingRecords());
            this.getSession().setAttribute("iframeresult",result);
            return mav;

            } catch (Exception sfe) {

               mav.addObject("status","CANTCONNECT");   
               return mav;

            }

         } else if (function!=null) {

            if (function.equalsIgnoreCase("keyvaluesub")) {
               // 3) Display matching records
               createKeyWhere();
               safeRedirect("showformrecords.do");
               return null;

            } else if (function.equalsIgnoreCase("recordsel")) {

               getEditRecord();   
               return null;

            } else if (function.equalsIgnoreCase("deleterecord")) {

               deleteRecord();   
               mav.addObject("status","DELETED");
               return mav;

            } 

         } else if (dtdefid!=null && formtypeid!=null) {

            // Initialize options if necessary
            auth.initOption("holdopt","N");
            auth.initOption("auditopt","N");

            // 1) Initial call
            auth.setValue("dtdefid",dtdefid);
            auth.setValue("formtypeid",formtypeid);
            safeRedirect("editformrecord.do");

         } else {

            // 2) Display keyvar form
            dtdefid=auth.getValue("dtdefid");
            formtypeid=auth.getValue("formtypeid");

            List fkl=getMainDAO().execQuery(
               "select f from Datatablecoldef f where f.datatabledef.dtdefid=" + dtdefid + 
                   " and f.iskeyfield=true " +
                   "order by f.columnorder"
               );

            ArrayList keylist=new ArrayList();
            Iterator i=fkl.iterator();
            while (i.hasNext()) {
               HashMap map=new HashMap();
               map.put("datatablecoldef",(Datatablecoldef)i.next());
               map.put("colvalue","");
               keylist.add(map);
            }

            // pre-fill value where possible and set to readonly for external users
            if (auth.getValue("rightslevel").equalsIgnoreCase("EXTERNAL")) {

               externalIdAssign(keylist,dtdefid);

            }

            mav.addObject("status","KEYVARINFO");   
            mav.addObject("keyvarlist",keylist);   
            return mav;

         }

         return null;

      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   public void createKeyWhere() throws FORMSException, FORMSKeyException, FORMSSecurityException {

      // Construct keyvar component of where clause from parameter values
      keycount=0;
      Map parms=getRequest().getParameterMap();
      Iterator i=parms.keySet().iterator();
      StringBuilder sbwhere=new StringBuilder();
      while ( i.hasNext() ) {
        String key= (String)i.next();
        if (!key.equals("_function_")) {
           String value=((String[])parms.get( key ))[ 0 ];
           keycount++;
           if (value!=null && value.length()>0) {
              sbwhere.append(" (values.varname='" + key + "' and " +
                               "values.varvalue='" + value + "') or ");
           } else {
              sbwhere.append(" (values.varname='" + key + "') or ");
           }
        }
      }

      String stwhere="";
      if (sbwhere.length()>0) {
         stwhere=sbwhere.toString().substring(0,sbwhere.toString().lastIndexOf(" or "));
      }
      getAuth().setValue("stwhere",stwhere);

   }   

   public List getMatchingRecords() throws FORMSException, FORMSKeyException, FORMSSecurityException {

      FORMSAuth auth=getAuth();
      String stwhere=auth.getValue("stwhere");
      String dtdefid=auth.getValue("dtdefid");

      // See if form is a remote site form
      boolean isremoteform=false;
      String qdtdefid;
      Object[] oarray=(Object[])getMainDAO().execUniqueQuery(
            "select f.allinst.ainstid,f.remotedtdefid from Datatabledef f " +
               "where f.dtdefid=" + dtdefid +  "and f.allinst.islocal!=true"
               );
      if (oarray!=null) {
         isremoteform=true;
         qdtdefid=((Long)oarray[1]).toString();
      } else {
         qdtdefid=dtdefid;
      }

      // create query string
      StringBuilder sbquery=new StringBuilder(   
         "select values from " +
            "Datatablevalues as values " +
            "inner join fetch values.datatablerecords as records " +
            "inner join fetch records.datatabledef as datatabledef " +
            "inner join datatabledef.datatablecoldef as columns " +
            "where (" + stwhere + ") and " + 
               "(datatabledef.dtdefid=" + qdtdefid + ") and " +
               "(values.varname=columns.columnname) " 
         );
      if (!auth.getValue("holdopt").equals("Y")) {   
         sbquery.append(" and (records.ishold!=true) ");
      }         
      if (!auth.getValue("auditopt").equals("Y")) {   
         sbquery.append(" and (records.isaudit!=true) ");
      }         
      sbquery.append(" order by records.datarecordid,columns.columnorder");

      // Return matching records
      List l;
      
      if (!isremoteform) {
         // Local form query submission
         l=execServerQuery(sbquery.toString());
         auth.setObjectValue("formloc",LockInfo.LOCALFORM);
         auth.setObjectValue("forminst",null);
      } else {
         // pull linkedinst record for submission
         Linkedinst forminst=(Linkedinst)getMainDAO().execUniqueQuery("select a.linkedinst from Allinst a where a.ainstid=" + (Long)oarray[0]);
         l=execRemoteServerQuery(forminst,sbquery.toString());
         auth.setObjectValue("formloc",LockInfo.REMOTEFORM);
         auth.setObjectValue("forminst",forminst);
      }

      // Reorganize data into single row form and create list of ids to keep (based
      //    on having records for all key fields)
      Iterator i=l.iterator();
      long previd=-1;
      int currcount=1;
      ArrayList keeplist=new ArrayList();
      SortMap rmap=new SortMap();
      ArrayList rlist=new ArrayList();
      StringBuilder infosb=new StringBuilder();
      while (i.hasNext()) {
         Datatablevalues fdv=(Datatablevalues)i.next();
         long currid=fdv.getDatatablerecords().getDatarecordid();
         if (currid==previd) {
            currcount++;
            infosb.append(fdv.getVarname() + "=" + fdv.getVarvalue() + ", ");
            rmap.put(fdv.getVarname(),fdv.getVarvalue());
         } else {
            currcount=1;
            rmap=new SortMap();
            infosb=new StringBuilder();
            infosb.append(fdv.getVarname() + "=" + fdv.getVarvalue() + ", ");
            rmap.put(fdv.getVarname(),fdv.getVarvalue());
         } 
         if (currcount==keycount) {
            Datatablerecords fdr=fdv.getDatatablerecords();
            // get saveusername and save to lockinfo for purposes of display
            fdr.setLockinfo(getUserName(isremoteform,fdr.getSaveuser()));
            keeplist.add(new Long(currid));
            rmap.put("_datarecord_",fdr);
            rmap.put("__showhold",auth.getOption("holdopt"));
            rmap.put("__showaudit",auth.getOption("auditopt"));
            rmap.put("__infostring",infosb.deleteCharAt(infosb.lastIndexOf(",")).toString());
            rlist.add(rmap);
         }
         previd=currid;
      }

      // remove records not in keeplist
      i=rlist.iterator();
      while (i.hasNext()) {
         SortMap map=(SortMap)i.next();
         Datatablerecords fdr=(Datatablerecords)map.get("_datarecord_");
         long currid=fdr.getDatarecordid();
         boolean keepthis=false;
         Iterator k=keeplist.iterator();
         while (k.hasNext()) {
            Long compval=(Long)k.next();
            if (compval.longValue()==currid) keepthis=true;
         }
         if (!keepthis) i.remove();
      }  

      Collections.sort(rlist);

      return rlist;

   }

   // Create Comparable map class for output
   private class SortMap extends LinkedHashMap implements Comparable {

      public int compareTo(Object other) {
         try {
            SortMap omap=(SortMap)other;
            Set tset=this.entrySet();
            Set oset=omap.entrySet();
            Iterator ti=tset.iterator();
            while (ti.hasNext()) {
               Map.Entry tentry=(Map.Entry)ti.next();
               String tkey=(String)tentry.getKey();
               if (tkey==null || tkey.equalsIgnoreCase("_datarecord_") || tkey.indexOf("__")>=0) {
                  continue;
               }
               String tv=tentry.getValue().toString();
               // do not order dates, use savetime instead
               if (tv.indexOf("/")>0) {
                  continue;
               }
               Iterator oi=oset.iterator();
               while (oi.hasNext()) {
                  Map.Entry oentry=(Map.Entry)oi.next();
                  if (tkey.equals((String)oentry.getKey())) {
                     String ov=oentry.getValue().toString();
                     // do not order dates, use savetime instead
                     if (ov.indexOf("/")>0) {
                        continue;
                     }
                     try {

                        if ((new Long(tv).longValue())>(new Long(ov).longValue())) {
                           return +1;
                        } else if ((new Long(tv).longValue())<(new Long(ov).longValue())) {
                           return -1;
                        }
                     } catch (Exception ee) {
                        int cvalue=tv.compareTo(ov);
                        if (cvalue!=0) {
                           return cvalue;
                        }
                     }
                     break;
                  }
               }
            }
            Datatablerecords trec=(Datatablerecords)this.get("_datarecord_");
            Datatablerecords orec=(Datatablerecords)omap.get("_datarecord_");
            if (orec.getSavetime().after(trec.getSavetime())) {
               return -1;
            } else {
               return +1;
            }
         } catch (Exception e) {
            return -1;
         }   
      }

   }

   // delete selected record
   public void deleteRecord() throws FORMSException, FORMSKeyException, FORMSSecurityException {
      getEditRecord(true);
   }

   // retrieve selected record into form
   public void getEditRecord() throws FORMSException, FORMSKeyException, FORMSSecurityException {
      getEditRecord(false);
   }

   // Displays requested record in form
   public void getEditRecord(boolean isdelete) throws FORMSException, FORMSKeyException, FORMSSecurityException {

      FORMSAuth auth=getAuth();

      // pull record values
      List vlist;

      int formloc=((Integer)auth.getObjectValue("formloc")).intValue();
      Linkedinst forminst=null;
      if (formloc==LockInfo.REMOTEFORM) {
         forminst=(Linkedinst)auth.getObjectValue("forminst");
      } 
      long datarecordid=new Long(request.getParameter("datarecordid")).longValue();

      // Obtain record lock, if possible
      Datatablerecords rec=null;
      boolean islocked=false;
      boolean lockcleared=false;
      String clearuser="";
      boolean lockobtained=false;
      String lockinfo="";

      // pull record && see who input it
      int whoinput=SOMEONE_ELSE;
      if (formloc==LockInfo.LOCALFORM) {
         String querystring="select f from Datatablerecords f where f.datarecordid=" + datarecordid;
         Object[] oarray=execUniqueServerQuery(querystring);
         rec=(Datatablerecords)oarray[0];
         if (rec.getSaveuser()==(new Long(auth.getValue("auserid")).longValue())) {
            whoinput=SELF;
         }
      } else {
         CsaRetrieveRecordParms parms=new CsaRetrieveRecordParms("csaRetrieveRecordServiceTarget","getPatientRecord");
         parms.setDatarecordid(new Long(datarecordid));
         parms.setForminst(forminst);
         RetrieveRecordServiceResult result=
            (RetrieveRecordServiceResult)submitServiceRequest(parms);
         if (result.getStatus()==FORMSServiceConstants.OK) {
            rec=result.getRecord();
            if (result.getIsInputUser()) {
               whoinput=SELF;
            }
         }   
      }

      // construct form javascript from values    
      String dtdefid=auth.getValue("dtdefid");
      String formtypeid=auth.getValue("formtypeid");

      // Make sure user has permission to read form
      if (!(getPermHelper().hasFormPerm(dtdefid,"READOWN") && getPermHelper().hasFormPerm(dtdefid,"READOTHER"))) {
         // check permissions based on who input
         switch(whoinput) {
            case SELF:
               // input by self
               if (!getPermHelper().hasFormPerm(dtdefid,"READOWN")) {
                  safeRedirect("permission.err");
                  return;
               }
               break;
            case SOMEONE_ELSE:
               // input by someone else
               if (!getPermHelper().hasFormPerm(dtdefid,"READOTHER")) {
                  safeRedirect("permission.err");
                  return;
               }
               break;
         }
      }

      // Information map to be passed to service target
      HashMap map=new HashMap();
      map.put("formloc",formloc);
      map.put("forminst",forminst);
      map.put("record",rec);
      map.put("auserid",getAuth().getValue("auserid"));

      if (!rec.getIslocked()) {
         LockInfo linfo=new LockInfo();
         boolean isok=false;
         CsaServiceStatusResult result=(CsaServiceStatusResult)submitServiceRequest("csaRecordLockingServiceTarget","lockRecord",map);
         if (result.getStatus()==FORMSServiceConstants.OK) {
            linfo.setIslocked(true);
            linfo.setLinkedinst(forminst);
            linfo.setFormlocation(formloc);
            linfo.setDatarecordid(datarecordid);
            getAuth().setObjectValue("lockinfo",linfo);
            lockobtained=true;
         } 
      } else {

         islocked=true;
         boolean isok=false;
         LockInfo linfo=new LockInfo();
         // If lock has been held > 2hrs, clear it and issue warning
         if ((System.currentTimeMillis()-rec.getLocktime().getTime())>(1000*60*60*2)) {
            clearuser=rec.getLockinfo();
            // release current lock
            CsaServiceStatusResult result=(CsaServiceStatusResult)submitServiceRequest("csaRecordLockingServiceTarget","unlockRecord",map);
            if (result.getStatus()==FORMSServiceConstants.OK) {
               lockcleared=true;
               islocked=false;
            }   
            result=(CsaServiceStatusResult)submitServiceRequest("csaRecordLockingServiceTarget","lockRecord",map);
            if (result.getStatus()==FORMSServiceConstants.OK) {
               linfo.setIslocked(true);
               linfo.setLinkedinst(forminst);
               linfo.setFormlocation(formloc);
               linfo.setDatarecordid(datarecordid);
               getAuth().setObjectValue("lockinfo",linfo);
               lockobtained=true;
            } 
         }
         lockinfo=rec.getLockinfo();
      }

      if (isdelete) {
         // make sure user has permission
         if (!canEditRecord(dtdefid,whoinput)) {
            safeRedirect("permission.err");
            return;
         }
         // if permission, then perform delete
         
         CsaDeleteRecordParms parms=new CsaDeleteRecordParms("csaDeleteRecordServiceTarget","deleteRecord");
         parms.setRecord(rec);
         parms.setFormloc(formloc);
         parms.setForminst(forminst);
         submitServiceRequest(parms);
         return;

      }

      String querystring="select v from Datatablevalues v where v.datatablerecords.datarecordid=" + 
                         request.getParameter("datarecordid");
      if (auth.getObjectValue("formloc").equals(LockInfo.LOCALFORM)) {
         vlist=execServerQuery(querystring);
      } else {  
         vlist=execRemoteServerQuery(forminst,querystring);
      }   

/*
      // NOTE:  There is implicit trust for admin users here.  Even for forms that don't
      // exist or for forms from another study.  We may want to check that the form is
      // under the current study later 
      if (!getPermHelper().hasFormPerm(dtdefid,"NEWRECORD")) {

         mav.addObject("status","NOPERMISSION");
         return mav;

      }
*/

      // Display form
      Formstore fstore=(Formstore)getMainDAO().execUniqueQuery(
         "select f from Formstore f where f.formtypes.formtypeid=" + formtypeid
         );
      //StringBuilder fcontent=new StringBuilder(fstore.getFormcontent());   
      // TEMPORARY TO ALLOW COMPATIBILITY WITH FORMS STORED PRE-VERSION 0.5.0
      StringBuilder fcontent=new StringBuilder(fstore.getFormcontent().replaceFirst("\"formdefid_\"","\"dtdefid_\""));   


      response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
      response.setHeader("Pragma","no-cache"); //HTTP 1.0
      response.setDateHeader ("Expires", -1); //prevents caching at the proxy server

      /*************************************************************/
      /** RETRIEVE VALUES INTO JAVASCRIPT VARIABLES AND OPEN FILE **/
      /*************************************************************/

      ///////////////////////////////////////
      // ADD JAVASCRIPT TO PDF FILE STREAM //
      ///////////////////////////////////////
   
      // Create LoadScript
      StringBuilder ls=new StringBuilder();    

      ls.append("_numErrors=0;\n");
      ls.append("try { setReadOnly(getFormElementById(\"FormDataOK\")); } catch (e) {}\n");

      String _webProtocol=request.getRequestURL().toString();
      _webProtocol=_webProtocol.substring(0,_webProtocol.indexOf(":"));

      // THIS MUST BE LAST, SO WE ARE NOT SETTING PASSVALUE FIELD FROM FILE THAT WAS READ IN
      // Contains server information passed to form
      JSONObject passv=new JSONObject();
      passv.put("submittype","editrecord");
      passv.put("rightslevel",auth.getValue("rightslevel"));
      passv.put("servername",request.getServerName());
      passv.put("continueservlet","selectform.do");
      ls.append("try { _rmsPassValueField='" + passv.toString() + "'; _rmsPassValueObject=jsonParse(_rmsPassValueField); } catch (e) {}\n");

      // get form data values
      Iterator viter=vlist.iterator();
      while (viter.hasNext()) {
         Datatablevalues dvalue=(Datatablevalues)viter.next();
         ls.append("try { setFormValue(getFormElementById(\"" + dvalue.getVarname() + "\"),'" + dvalue.getVarvalue() + "'); } catch (e) {}\n");
      }

      ls.append("try { getFormElementById(\"dtdefid_\").value='" + dtdefid + "'; } catch (e) { }\n");
      ls.append("_docReady=0;\n");
      ls.append("try { execValidate(); } catch (e) {}\n");
      ls.append("_saveStatus=\"OK\";\n");
      ls.append("_docReady=1;\n");

      if (lockcleared) {
         ls.append("alert('WARNING:  This record was locked by user:  \\n\\n    ' + \n'" +
           clearuser + "\\n\\nHOWVER this lock has expired and has been cleared.    ');"); 
      }     
      if (islocked) {
         ls.append("alert('This record is currently locked by user:  \\n\\n    ' + \n'" +
           lockinfo + "\\n\\nYou will be able to view this form, but not submit it.   ');");
         ls.append("getFormElementById('SubmitButton').disabled='true';");  
      } else if (!lockobtained) {
         ls.append("alert('WARNING:  Record lock could not be obtained.  You may submit this form, ' + \n" +
             "' but submission may not be successful if a lock is held by another user.\\n\\n ' + \n" +
             "'In any case an FORMS Temporary Record will be created allowing for later submission');");
      }

      // make sure user has permission to edit record
      if (!canEditRecord(dtdefid,whoinput)) {
         ls.append("alert('Your account lacks the required permission to edit this record.  You will be able to view this form, but not submit it');\n\n");
         ls.append("getFormElementById('SubmitButton').disabled='true';");  
         ls.append("getFormElementById('ScoreButton').disabled='true';");  
         ls.append("getFormElementById('ValidateButton').disabled='true';");  
         ls.append("getFormElementById('ResetButton').disabled='true';");  
         ls.append("getFormElementById('LocalButton').disabled='true';");  
      }

      //auth.setValue("WinNameMissOk","Y");

      String rstr="_insertLoadScriptHere=\"XXXHEREXXX\";";
      int inx=fcontent.indexOf(rstr);
      int inxt =inx+rstr.length();
      String outstring=fcontent.replace(inx,inxt,ls.toString()).toString();
      out.println(outstring);
      out.flush();

   }

   // Create JavaScript ID/ECODE assignments for external users if appropriate
   private void externalIdAssign(ArrayList keylist,String dtdefid) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      StringBuilder sb=new StringBuilder();

      FORMSAuth auth=getAuth();

      // See if an ID assignment exists for user
      List ulist=getMainDAO().execQuery( "select u from Useridassign u where u.studies.studyid=" +  auth.getValue("studyid") +
         " and u.localusers.luserid=" + auth.getValue("luserid")
         );

      // update keyvalue map for assignments
      Iterator ki=keylist.iterator();
      while (ki.hasNext()) {
         HashMap map=(HashMap)ki.next();
         Datatablecoldef f=(Datatablecoldef)map.get("datatablecoldef");
         Iterator ui=ulist.iterator();
         while (ui.hasNext()) {
            Useridassign u=(Useridassign)ui.next();
            if (u.getVarname().equalsIgnoreCase(f.getColumnname())) {
               map.put("colvalue",u.getVarvalue());
            }
         }

      }
  
   }

   // See if user has permission to edit record   
   private boolean canEditRecord(String dtdefid,int whoinput) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      if (!(getPermHelper().hasFormPerm(dtdefid,"EDITOWN") && getPermHelper().hasFormPerm(dtdefid,"EDITOTHER"))) {
         // check permissions based on who input
         switch(whoinput) {
            case SELF:
               // input by self
               if (!getPermHelper().hasFormPerm(dtdefid,"EDITOWN")) {
                  return false;
               }
               break;
            case SOMEONE_ELSE:
               // input by someone else
               if (!getPermHelper().hasFormPerm(dtdefid,"EDITOTHER")) {
                  return false;
               }
               break;
         }
      }
      return true;
   }

   private String getUserName(boolean isremoteform,long auserid) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      try {
         if (namemap==null) {
            namemap=new HashMap();
            // create name map
            List ulist=new ArrayList();
            if (!isremoteform) {
               ulist=getMainDAO().execQuery("select a from Allusers a");
            } else {
               Linkedinst forminst=(Linkedinst)getAuth().getObjectValue("forminst");
               ulist=execRemoteServerQuery(forminst,"select a from Allusers a");
            }
            Iterator i=ulist.iterator();
            while (i.hasNext()) {
               Allusers user=(Allusers)i.next();
               namemap.put(new Long(user.getAuserid()),user.getUserdesc().replaceFirst("\\[LOCAL\\]",""));
            }   
         }
         return (String)namemap.get(new Long(auserid));
      } catch (Exception e) {
         return new Long(auserid).toString();
      }

   }

}




