 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.csa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.io.*;
import java.util.*;
import java.text.*;
//import org.apache.regexp.RE;
//import org.apache.commons.lang.StringUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.ArrayUtils.*;
//import java.util.regex.*;
import javax.crypto.*;
import javax.crypto.spec.*;
import java.security.*;
import java.security.spec.*;
import uk.ltd.getahead.dwr.WebContext;
import uk.ltd.getahead.dwr.WebContextFactory;
import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.velocity.app.*;
import org.apache.velocity.*;
import org.apache.commons.httpclient.util.URIUtil;


public class ProcessFormDwr extends FORMSCsaServiceClientDwr {

   // Main record submit function
   public String submitRequest(String inString) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      try {

         FORMSAuth auth=this.getAuth();

         String rtnval="";

         // perform initial transformations of form output data
         String trString=FormDataUtil.transformDataStream(inString);
   
         // GRAB dtdefid_
         String dtdefid_=FormDataUtil.getFieldValue(trString,"dtdefid_");


   	 // Create RMS Temporary Record
         Long currtime=new Long(System.currentTimeMillis());

         Long auserid;
         try {
            auserid=new Long(auth.getValue("auserid"));
         } catch (Exception aue) {
            auserid=new Long(-999999);
         }
   
         String tempFileName = "t_" + dtdefid_  + "_" + auserid  + "_" + 
            currtime.toString() + ".xml.encrypt";

         String trencryptString=EncryptUtil.encryptString(trString,SessionObjectUtil.getInternalKeyObj(getSession()));

         MiscUtil.writeStringToFile(this.getSyswork().toString() + File.separator + tempFileName,trencryptString);

         String syncFilePath = getSession().getServletContext().getRealPath("/") + File.separator + 
                "SyncFiles" + File.separator + "s_" + dtdefid_  + "_" + auth.getValue("auserid")  + "_" + 
                currtime.toString() + ".xml.encrypt";


         /////////////////////////////////
         // check ecode if there is one //
         /////////////////////////////////

         List fkl=getMainDAO().execQuery(
            "select f from Datatablecoldef f join fetch f.datatabledef where f.datatabledef.dtdefid=" + dtdefid_ +
                " and f.iskeyfield=true " +
                "order by f.columnorder"
            );
         List ecl=getMainDAO().execQuery(
            "select f from Datatablecoldef f join fetch f.datatabledef where f.datatabledef.dtdefid=" + dtdefid_ +
                " and f.isecode=true " +
                "order by f.columnorder"
            );

         String ecodeVar=null;
         long ecodeVal=-1;
         if (ecl.size()>0) {
            try {
               ecodeVar=((Datatablecoldef)ecl.get(0)).getColumnname();
               ecodeVal=new Long(FormDataUtil.getFieldValue(trString,ecodeVar)).longValue();


            } catch (Exception e) {
               ecodeVar=null;
            }
         }

         Iterator fki=fkl.iterator();
         ArrayList keyvarlist=new ArrayList();
         ArrayList idvarlist=new ArrayList();
         // Create input string for ecode value comparison
         StringBuilder ecodesb=new StringBuilder();
         while (fki.hasNext()) {
            Datatablecoldef fcd=(Datatablecoldef)fki.next();
            String kvname=fcd.getColumnname().toLowerCase();
            String kvvalue=FormDataUtil.getFieldValue(trString,kvname);
            HashMap keyvarmap=new HashMap();
            keyvarmap.put("keyvar",kvname.toUpperCase());
            keyvarmap.put("keyval",kvvalue);
            keyvarlist.add(keyvarmap);
            if (fcd.getIsidfield()) {
               idvarlist.add(keyvarmap);
               ecodesb.append(kvvalue + "-");
            }
         }
         if (ecodesb.indexOf("-")>0) {
            ecodesb.deleteCharAt(ecodesb.lastIndexOf("-"));
         } 

         // Check ecode
         if (ecodeVar!=null) {
            if (new Long(EcodeUtil.getEcode(ecodesb.toString())).longValue()!=ecodeVal) {
                rtnval="ECODEPROB:::" + ecodesb.toString();
               if (getAuth().getValue("worklocally").equals("T")) {
                  getAuth().setValue("trencryptString",trencryptString);
                  getAuth().setValue("syncFilePath",syncFilePath);
               }
               return rtnval;
               
            }
         }

         // If no connection to server or local preference is specified, save record to SYNC directory   
         if (getAuth().getValue("worklocally").equals("T")) {

            // Write Sync directory record
            return writeSyncRecord(syncFilePath,trencryptString);

         }

         try {

         // Send records to service target
         CsaFormRecordProcessorParms parms=new CsaFormRecordProcessorParms();
         parms.setDatastring(trString);
         parms.setDtdefid(new Long(dtdefid_));
         parms.setAuserid(new Long(getAuth().getValue("auserid")));
         parms.setBeanname("formRecordProcessorServiceTarget");
         parms.setMethodname("submitRecord");

         FormRecordProcessorResult result=(FormRecordProcessorResult)this.submitServiceRequest(parms);
         int transferstatus=result.getTransferstatus();
         
         if (transferstatus==FormRecordProcessorConstants.RECORD_LOCKED) {
      
            // Save to sync directory
            return writeSyncRecord(syncFilePath,trencryptString);
      

         } else if (transferstatus==FormRecordProcessorConstants.ERROR || transferstatus==FormRecordProcessorConstants.WRITE_TO_SYNC) {

            // Save to sync directory
            return writeSyncRecord(syncFilePath,trencryptString);

         }
         
         String fieldstr=new FormRecordProcessorConstants().getFieldName(transferstatus);
         if (fieldstr.equals("ALREADY_EXISTS")) {
            fieldstr="ALREADY:::" + result.getInfostring();
         } else if (fieldstr.equals("NOENROLLMENT")) {
            fieldstr="NOENROLLMENT:::" + result.getInfostring();
         } else if (fieldstr.equals("ECODEPROB")) {
            fieldstr="ECODEPROB:::" + result.getInfostring();
         } 

         return fieldstr;

         } catch (Exception sex) {

            // Save to sync directory
            return writeSyncRecord(syncFilePath,trencryptString);

         }

      } catch (Exception e) {

         return "BADERROR";

      } 

   } 

   private String writeSyncRecord(String syncFilePath,String trencryptString) {

      MiscUtil.writeStringToFile(syncFilePath,trencryptString);

      if (new File(syncFilePath).exists()) {
         return "SAVEDSYNC";
      } else {
         return "SYNCERROR";
      }

   } 

   // Replace existing record
   public String submitReplace() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      try {

         // Don't need to resend datastring, use session-maintained one
         CsaServiceSystemParms parms=new CsaServiceSystemParms();
         parms.setBeanname("formRecordProcessorServiceTarget");
         parms.setMethodname("submitReplace");
         FormRecordProcessorResult result=(FormRecordProcessorResult)this.submitServiceRequest(parms);
         int transferstatus=result.getTransferstatus();
         
               
         if (transferstatus==FormRecordProcessorConstants.RECORD_LOCKED) {
            return "ERROR";
         } else if (transferstatus==FormRecordProcessorConstants.ERROR) {
            return "ERROR";
         } 
         return new FormRecordProcessorConstants().getFieldName(transferstatus);
         
      } catch (Exception e) {

         return "ERROR";

      }

   }

   // Submit even without matching enrollment record 
   public String submitAnyway() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      try {

         // Don't need to resend datastring, use session-maintained one
         CsaServiceSystemParms parms=new CsaServiceSystemParms();
         parms.setBeanname("formRecordProcessorServiceTarget");
         parms.setMethodname("submitAnyway");
         FormRecordProcessorResult result=(FormRecordProcessorResult)this.submitServiceRequest(parms);
         int transferstatus=result.getTransferstatus();
         
               
         if (transferstatus==FormRecordProcessorConstants.RECORD_LOCKED) {
            return "ERROR";
         } else if (transferstatus==FormRecordProcessorConstants.ERROR) {
            return "ERROR";
         } 
         return new FormRecordProcessorConstants().getFieldName(transferstatus);
         
      } catch (Exception e) {

         return "ERROR";

      }

   }

   // Submit record to hold database
   public String submitHold() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      try {

         // If no connection to server or local preference is specified, save record to SYNC directory   
         if (getAuth().getValue("worklocally").equals("T")) {

            // Write Sync directory record
            return writeSyncRecord(getAuth().getValue("syncFilePath"),getAuth().getValue("trencryptString"));

         }

         // Don't need to resend datastring, use session-maintained one
         CsaServiceSystemParms parms=new CsaServiceSystemParms();
         parms.setBeanname("formRecordProcessorServiceTarget");
         parms.setMethodname("submitHold");

         FormRecordProcessorResult result=(FormRecordProcessorResult)this.submitServiceRequest(parms);
         int transferstatus=result.getTransferstatus();

         if (transferstatus==FormRecordProcessorConstants.RECORD_LOCKED) {
            return "ERROR";
         } else if (transferstatus==FormRecordProcessorConstants.ERROR) {
            return "ERROR";
         } 
         return new FormRecordProcessorConstants().getFieldName(transferstatus);
         
      } catch (Exception e) {

         return "ERROR";

      }
      
   }


   // Perform interim save
   public String interimSave(String inString) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      try {

         FORMSAuth auth=this.getAuth();

         String rtnval="";

         // perform initial transformations of form output data
         String trString=FormDataUtil.transformDataStream(inString);
   
         // GRAB dtdefid
         Long dtdefid=new Long(FormDataUtil.getFieldValue(trString,"dtdefid_"));

         Long auserid;
         try {
            auserid=new Long(auth.getValue("auserid"));
         } catch (Exception aue) {
            auserid=new Long(-999999);
         }

   	 // Create RMS Temporary Record

         Long currtime=new Long(System.currentTimeMillis());
   
         String tempFileName = "i_" + dtdefid  + "_" + auserid  + "_" + 
            currtime + ".xml.encrypt";

         MiscUtil.writeStringToFile(this.getSyswork().toString() + File.separator + tempFileName,
                 EncryptUtil.encryptString(trString,SessionObjectUtil.getInternalKeyObj(getSession()))
            );

         rtnval="INTERIMSAVEOK";
         return rtnval;

      } catch (Exception e) {

         return "BADERROR";

      }
      
   }

}


