 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/* 
 *
 * SyncFormDataController.java -  CSA Database Setup Program
 *
 *   This class extends FORMSServiceClientController rather than FORMSCsaServiceClientController because it
 *   is a non-standard CSA controller in the sense that it is called outside a CSA
 *   logged in session
 *
 */

package com.owarchitects.forms.csa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.lang.*;
import org.springframework.web.servlet.*;
import java.net.*;
import javax.servlet.http.*;
import javax.crypto.SecretKey;

public class SyncFormDataController extends FORMSServiceClientController {

   private boolean isRunning=false;

   public ModelAndView submitRequest() throws FORMSException {

      try {

         String syncdirpath = getSession().getServletContext().getRealPath("/") + File.separator + 
                "SyncFiles";

         if (!isRunning) {
            SyncFormDataThread proc=new SyncFormDataThread(getRequest(),getResponse(),getSession(),getAuth(),getSyswork().getAbsolutePath(),
               getMainDAO(),SessionObjectUtil.getInternalKeyObj(getSession()),syncdirpath,new Long(getAuth().getValue("auserid")));
            proc.start();
         }   
   
         mav.addObject("status","THREAD");
         return mav;

      } catch (Exception e) {
         // Don't perform update
         safeRedirect("selectstudy.do");
         return null;
      }

    }

    // SyncFormDataThread - Performs syncing as a Thread

    private class SyncFormDataThread extends FORMSCsaServiceClientThread {

       com.owarchitects.forms.commons.db.MainDAO maindao=null;
       IframeReloaderResult result;
       StringBuilder sb;
       String syncdirpath;
       Long auserid;
       ServerSocket runsocket;
       SecretKey ikey;

       public SyncFormDataThread(HttpServletRequest request,HttpServletResponse response,HttpSession session,FORMSAuth auth,String syswork,
          com.owarchitects.forms.commons.db.MainDAO maindao,SecretKey ikey,String syncdirpath,Long auserid) {
          super(request,response,session,auth,syswork);
          this.maindao=maindao;
          this.syncdirpath=syncdirpath;
          this.auserid=auserid;
          this.ikey=ikey;
       }

       public void run() {

          if (isRunning) return;

          isRunning=true;
          sb=new StringBuilder();
          result=new IframeReloaderResult();
          // Only need to set this once, since iframeresult will hold reference to object
          // rather than copy of it
          getSession().setAttribute("iframeresult",result);

          try {

             result.setStatus("RELOAD");

             sbWrite("\n<span style='color:#00AA00'><br>BEGIN SYNCING PROCESS:<br><br></span>\n");
             sbWrite("Verifying socket access\n");

             // Keep this socket open for process duration so multiple processes can't run.
             try {
                runsocket=new ServerSocket(54441);
             } catch (Exception e) {
                sbWrite("<br>CANNOT ACCESS SOCKET CONNECTION. &nbsp;ANOTHER SYNCING PROCESS MAY BE " +
                        "RUNNING. &nbsp;PROCESS WILL BE CANCELLED.<br><br>\n");
                stopNow(false);
                return;
             }

             // This socket must be accessible to process queries to user
             try {
                ServerSocket socket=new ServerSocket(54444);
                socket.close();
                sbWrite(".....DONE<br><br>\n");
             } catch (Exception e) {
                sbWrite("<br>CANNOT ACCESS SOCKET CONNECTION. &nbs;PROCESS WILL BE CANCELLED. &nbsp;TRY AGAIN LATER.<br><br>\n");
                stopNow(false);
                return;
             }

             sbWrite("Verifying connection to FORMS SSA\n");

             // verify connection to server
             boolean canconnect=false;
             CsaServiceSystemParms parms=new CsaServiceSystemParms();
             parms.setBeanname("verifyConnectionServiceTarget");
             parms.setMethodname("checkStatus");
             try {
                CsaServiceStatusResult result=(CsaServiceStatusResult)this.submitServiceRequest(parms);
                if (result.getStatus()!=null && result.getStatus()==FORMSServiceConstants.OK) {
                   canconnect=true;
                }
             } catch (Exception conne) { 
                // For some reason, the first attempt in a session occasionally fails.  Try once more.
                sbWrite("<br>First attempt failed. &nbsp;Trying again.\n");
                Thread.sleep(1000);
                try {
                   CsaServiceStatusResult result=(CsaServiceStatusResult)this.submitServiceRequest(parms);
                   if (result.getStatus()!=null && result.getStatus()==FORMSServiceConstants.OK) {
                      canconnect=true;
                   }
                } catch (Exception conne2) {
                   sbWrite(Stack2string.getString(conne));
                }
             }

             if (canconnect) {
                sbWrite(".....DONE<br><br>\n");
             } else {
                sbWrite("<br>CANNOT CONNECT TO SSA. &nbsp;SYNCING PROCESS WILL BE CANCELLED.<br><br>\n");
                stopNow(false);
                return;
             }

             File syncdir=new File(syncdirpath);

             if (!syncdir.exists()) {
                sbWrite("<br>ERROR:  SYNC DIRECTORY DOES NOT EXIST!!! &nbsp;SYNCING PROCESS WILL BE CANCELLED.<br><br>\n");
                stopNow(false);
                return;
             }
             if (!syncdir.isDirectory()) {
                sbWrite("<br>" + syncdirpath + " IS NOT A DIRECTORY!!! &nbsp;SYNCING PROCESS WILL BE CANCELLED.<br><br>\n");
                stopNow(false);
                return;
             }

             sb.append(".....DONE<br><br>\n");
             sbWrite("See if there are any files to send:<br><br>\n");

             File[] lfna=syncdir.listFiles();

             int _cf=0;

             // Reverse sort list by file name
             // We will only use the most recent file for a formid keyvar combination
             // saved multiple times.  Others will be ignored.
             Arrays.sort(lfna,new ReverseSortFilesByName());

             Iterator i=Arrays.asList(lfna).iterator();
             while (i.hasNext()) {   
                File f=(File)i.next();
                if (f.isFile() && f.getName().toLowerCase().indexOf("xml.encrypt")>0) _cf++;
                else {
                   // remove file from list if possible
                   try {
                      i.remove();
                   } catch (UnsupportedOperationException uex) {
                      // do nothing
                   }
                }
             }
             sbWrite(_cf + " File(s) found:<br><br>" + Arrays.toString(lfna) + "<br><br>");
             if (_cf<1) {
                sbWrite("<br>No files to send.  Nothing done." + "<br><br>");
                stopNow();
                return;
             }

             sb.append(".....DONE<br><br>\n");
             sbWrite("Begin importing files:<br><br>\n");

             i=Arrays.asList(lfna).iterator();
             // Keep list of datatabledef/keyfield combinations (only the most recent save will be sent)
             ArrayList recordlist=new ArrayList();
             while (i.hasNext()) {   
                File f=(File)i.next();
                if (f.isFile() && f.getName().toLowerCase().indexOf("xml.encrypt")>0) {
                   sbWrite("<br>IMPORTING " + f.getName() + "<br>");
                   
                   // return auserid,dtdefid & submittime from filename
                   String[] narray=f.getName().split("[_.]");
                   long s_dtdefid=-99999999;
                   long s_auserid=-99999999;
                   long s_submittime=-99999999;
                   String cstring=null;
                   try {

                      sbWrite(Arrays.toString(narray) + "<br>");

                      s_dtdefid=new Long(narray[1]).longValue();
                      s_auserid=new Long(narray[2]).longValue();
                      s_submittime=new Long(narray[3]).longValue();

                      cstring=EncryptUtil.decryptString(MiscUtil.readFileToString(f),ikey);

                   } catch (Exception impex) {

                      sbWrite("<font color='#0000DD'>ERROR:  Filename is not valid.  File will be skipped.<br></font>\n");

                   }

                   // pull key field values for form (only the most recent form submission will be synced)

                   // get key field names
                   List klist=maindao.execQuery("select upper(f.columnname) from Datatablecoldef f " +
                                                     " where f.datatabledef.dtdefid=" + s_dtdefid + " and iskeyfield=true");
                   FormDataParser parser=new FormDataParser(cstring,false);
                   ArrayList kvaluelist=parser.getFieldList();
                   // keep only keyfield values
                   Iterator kiter=kvaluelist.iterator();
                   while (kiter.hasNext()) {
                      HashMap map=(HashMap)kiter.next();
                      if (!klist.contains(((String)map.get("fieldname")).toUpperCase())) {
                         kiter.remove();
                      }
                   }
                   // See if equivalent record has already been transfered
                   int nfieldmatches=0;
                   Iterator riter=recordlist.iterator();
                   while (riter.hasNext()) {
                      HashMap rmap=(HashMap)riter.next();
                      if (((Long)rmap.get("dtdefid")).longValue()==s_dtdefid) {
                         nfieldmatches=0;
                         ArrayList complist=(ArrayList)rmap.get("kvaluelist");
                         kiter=kvaluelist.iterator();
                         while (kiter.hasNext()) {
                            HashMap imap=(HashMap)kiter.next();
                            String fieldname=(String)imap.get("fieldname");
                            String fieldvalue=(String)imap.get("fieldvalue");
                            Iterator citer=complist.iterator();
                            while (citer.hasNext()) {
                               HashMap cmap=(HashMap)citer.next();
                               String c_fieldname=(String)cmap.get("fieldname");
                               String c_fieldvalue=(String)cmap.get("fieldvalue");
                               if (fieldname.equalsIgnoreCase(c_fieldname) && fieldvalue.equals(c_fieldvalue)) {
                                  nfieldmatches++;
                                  break;
                               }
                            }
                         }
                      }
                   }
                   if (nfieldmatches==kvaluelist.size()) {
                      sbWrite("<font color='#0000DD'>A record for this dtdefid/Keyfield combination has " +
                              "already been processed. &nbsp;This file will be skipped.<br></font>\n");
                      moveToSent(f);
                   } else {


                      // save recordinfo to recordlist
                      HashMap smap=new HashMap();
                      smap.put("dtdefid",s_dtdefid);
                      smap.put("kvaluelist",kvaluelist);
                      recordlist.add(smap);

                      // Send record data to SSA server

                      try {
             
                         // Send records to service target
                         CsaFormRecordProcessorParms sparms=new CsaFormRecordProcessorParms();
                         sparms.setDatastring(cstring);
                         sparms.setDtdefid(new Long(s_dtdefid));
                         sparms.setAuserid(auserid);
                         sparms.setBeanname("formRecordProcessorServiceTarget");
                         sparms.setMethodname("submitRecord");
             
                         FormRecordProcessorResult result=(FormRecordProcessorResult)this.submitServiceRequest(sparms);
                         int transferstatus=result.getTransferstatus();
                      
                         if (transferstatus==FormRecordProcessorConstants.ERROR) {
             
                            sbWrite("<font color='#0000DD'>Record could not be processed at this time.  This file will be skipped for now.<br></font>\n");
             
                         } else {
                      
                            String fieldstr=new FormRecordProcessorConstants().getFieldName(transferstatus);
                            if (fieldstr.equals("ALREADY_EXISTS")) {
                               fieldstr=fieldstr + ":::" + result.getInfostring() + ":::" + sb.toString();
                               String qstring=query(fieldstr);
                               if (qstring.equals("REPLACE")) {

                                  try {
                                     CsaFormRecordProcessorParms hparms=new CsaFormRecordProcessorParms();
                                     hparms.setDatastring(cstring);
                                     hparms.setDtdefid(new Long(s_dtdefid));
                                     hparms.setAuserid(auserid);
                                     hparms.setBeanname("formRecordProcessorServiceTarget");
                                     hparms.setMethodname("submitReplace");
                            
                                     FormRecordProcessorResult hresult=(FormRecordProcessorResult)this.submitServiceRequest(hparms);
                                     int htransferstatus=hresult.getTransferstatus();
                                     
                                     sbWrite("<font color='#0000DD'>" + new FormRecordProcessorConstants().getFieldName(htransferstatus) + "<br></font>\n");
                                     if (htransferstatus==FormRecordProcessorConstants.ERROR) {
                                        sbWrite("<font color='#0000DD'>ERROR:  Record transfer could not be completed at this time.  This file " +
                                                "will be skipped for now.<br></font>\n");
                                     } else if (htransferstatus==FormRecordProcessorConstants.REPLACED) {
                                        sbWrite("<font color='#0000DD'>Record successfully replaced!<br></font>\n");
                                        moveToSent(f);
                                     } 
                                  } catch (Exception e) {
                                     sbWrite("<font color='#0000DD'>ERROR:  Record transfer could not be completed at this time.  This file " +
                                             "will be skipped for now.<br></font>\n");
                                  }

                               } else if (qstring.equals("HOLD")) {

                                  try {
                                     CsaFormRecordProcessorParms hparms=new CsaFormRecordProcessorParms();
                                     hparms.setDatastring(cstring);
                                     hparms.setDtdefid(new Long(s_dtdefid));
                                     hparms.setAuserid(auserid);
                                     hparms.setBeanname("formRecordProcessorServiceTarget");
                                     hparms.setMethodname("submitHold");
                            
                                     FormRecordProcessorResult hresult=(FormRecordProcessorResult)this.submitServiceRequest(hparms);
                                     int htransferstatus=hresult.getTransferstatus();
                                     
                                     if (htransferstatus==FormRecordProcessorConstants.ERROR) {
                                        sbWrite("<font color='#0000DD'>ERROR:  Record transfer could not be completed at this time.  This " +
                                                "file will be skipped for now.<br></font>\n");
                                     } else if (htransferstatus==FormRecordProcessorConstants.HOLDAPPEND) {
                                        sbWrite("<font color='#0000DD'>Record successfully appended to HOLD dataset!<br></font>\n");
                                        moveToSent(f);
                                     } 
                                  } catch (Exception e) {
                                     sbWrite("<font color='#0000DD'>ERROR:  Record transfer could not be completed at this time.  This " +
                                             "file will be skipped for now.<br></font>\n");
                                  }

                               } else if (qstring.equals("SKIP")) {
                                  sbWrite("<font color='#0000DD'>Skipping record per request.<br></font>\n");
                               } 

                            } else if (fieldstr.equals("NOENROLLMENT")) {
                               fieldstr=fieldstr + ":::" + result.getInfostring() + ":::" + sb.toString();
                               String qstring=query(fieldstr);
                               if (qstring.equals("ANYWAY")) {

                                  try {
                                     CsaFormRecordProcessorParms hparms=new CsaFormRecordProcessorParms();
                                     hparms.setDatastring(cstring);
                                     hparms.setDtdefid(new Long(s_dtdefid));
                                     hparms.setAuserid(auserid);
                                     hparms.setBeanname("formRecordProcessorServiceTarget");
                                     hparms.setMethodname("submitAnyway");

                                     FormRecordProcessorResult hresult=(FormRecordProcessorResult)this.submitServiceRequest(hparms);
                                     int htransferstatus=hresult.getTransferstatus();
                                     
                                     sbWrite("<font color='#0000DD'>" + new FormRecordProcessorConstants().getFieldName(htransferstatus) + "<br></font>\n");
                                     if (htransferstatus==FormRecordProcessorConstants.ERROR) {
                                        sbWrite("<font color='#0000DD'>ERROR:  Record transfer could not be completed at this time.  This file " +
                                                "will be skipped for now.<br></font>\n");
                                     } else if (htransferstatus==FormRecordProcessorConstants.APPENDED) {
                                        sbWrite("<font color='#0000DD'>Record successfully appended to SSA dataset!<br></font>\n");
                                        moveToSent(f);
                                     } 
                                  } catch (Exception e) {
                                     sbWrite("<font color='#0000DD'>ERROR:  Record transfer could not be completed at this time.  This file " +
                                             "will be skipped for now.<br></font>\n");
                                  }

                               } else if (qstring.equals("HOLD")) {

                                  try {
                                     CsaFormRecordProcessorParms hparms=new CsaFormRecordProcessorParms();
                                     hparms.setDatastring(cstring);
                                     hparms.setBeanname("formRecordProcessorServiceTarget");
                                     hparms.setMethodname("submitHold");
                            
                                     FormRecordProcessorResult hresult=(FormRecordProcessorResult)this.submitServiceRequest(hparms);
                                     int htransferstatus=hresult.getTransferstatus();
                                     
                                     if (htransferstatus==FormRecordProcessorConstants.ERROR) {
                                        sbWrite("<font color='#0000DD'>ERROR:  Record transfer could not be completed at this time.  This " +
                                                "file will be skipped for now.<br></font>\n");
                                     } else if (htransferstatus==FormRecordProcessorConstants.HOLDAPPEND) {
                                        sbWrite("<font color='#0000DD'>Record successfully appended to HOLD dataset!<br></font>\n");
                                        moveToSent(f);
                                     } 
                                  } catch (Exception e) {
                                     sbWrite("<font color='#0000DD'>ERROR:  Record transfer could not be completed at this time.  This " +
                                             "file will be skipped for now.<br></font>\n");
                                  }

                               } else if (qstring.equals("SKIP")) {
                                  sbWrite("<font color='#0000DD'>Skipping record per request.<br></font>\n");
                               } 

                            } else if (fieldstr.equals("APPENDED")) {
                               sbWrite("<font color='#0000DD'>Record successfully appended to SSA dataset!<br></font>\n");
                               moveToSent(f);
                            } else if (fieldstr.equals("ECODEPROB")) {
                               fieldstr=fieldstr + ":::" + result.getInfostring() + ":::" + sb.toString();
                               String qstring=query(fieldstr);
                               if (qstring.equals("HOLD")) {

                                  try {
                                     CsaFormRecordProcessorParms hparms=new CsaFormRecordProcessorParms();
                                     hparms.setDatastring(cstring);
                                     hparms.setBeanname("formRecordProcessorServiceTarget");
                                     hparms.setMethodname("submitHold");
                            
                                     FormRecordProcessorResult hresult=(FormRecordProcessorResult)this.submitServiceRequest(hparms);
                                     int htransferstatus=hresult.getTransferstatus();
                                     
                                     if (htransferstatus==FormRecordProcessorConstants.ERROR) {
                                        sbWrite("<font color='#0000DD'>ERROR:  Record transfer could not be completed at this time.  This " +
                                                "file will be skipped for now.<br></font>\n");
                                     } else if (htransferstatus==FormRecordProcessorConstants.HOLDAPPEND) {
                                        sbWrite("<font color='#0000DD'>Record successfully appended to HOLD dataset!<br></font>\n");
                                        moveToSent(f);
                                     } 
                                  } catch (Exception e) {
                                     sbWrite("<font color='#0000DD'>ERROR:  Record transfer could not be completed at this time.  This " +
                                             "file will be skipped for now.<br></font>\n");
                                  }

                               } else if (qstring.equals("SKIP")) {
                                  sbWrite("<font color='#0000DD'>Skipping record per request.<br></font>\n");
                               } 
                            } else if (fieldstr.equals("NOTPERMITTED") || fieldstr.equals("ALREADY_NOTPERMITTED")) {
                               sbWrite("<font color='#0000DD'>Your account lacks permission to write this record.  It is being  skipped.<br></font>\n");
                            } else if (fieldstr.equals("RECORD_LOCKED")) {
                               sbWrite("<font color='#0000DD'>This record is currently locked on the server and cannot be updated.  It is being  skipped.<br></font>\n");
                            } else {
                               sbWrite("<font color='#0000DD'>Record could not be written to the server at this time (UNKNOWN CAUSE).  It is being  skipped.<br></font>\n");
                            }
                         }
                
                      } catch (Exception sex) {
                
                         sbWrite("<font color='#0000DD'>Record could not be processed at this time.  This " +
                                 "file will be skipped for now.<br></font>\n");
               
                      }

                   }
                   sb.append(".....DONE<br><br>\n");
                   // 
                }
             }

          } catch (Exception e) {

             sbWrite("<span style='color:#AA0000'><br><br>Warning!!! Process threw an exception. &nbsp;" +
                       "Please check URL and internet connection.<br><br><span style='color:#000000'>" +
                       Stack2string.getString(e) + "</span><br><br>" +
                       "<br><br></span>\n");
             stopNow(false);
             return;

          }

          stopNow(true);
          return;

       }

       private void stopNow(boolean isok) {
          try {
             runsocket.close();
          } catch (java.io.IOException jie) {
             // Do nothing
          }
          if (isok) {
             sb.append("<span style='color:#00AA00'>SYNCING PROCESS COMPLETED SUCCESSFULLY!<br><br></span>\n");
          } else {
             sb.append("<span style='color:#AA0000'>SYNCING PROCESS WAS UNSUCCESSFUL.<br><br></span>\n");
          }
          // Make ok button visible
          sb.append("<script>\n");
          sb.append("   var b=parent.document.getElementById('okbutn');\n");
          sb.append("   b.style.visibility='visible';\n");
          sbWrite("</script>\n");

          result.setStatus("RELOAD");
          result.setContent(sb.toString());
          safeSleep(500);
   
          result.setStatus("DONE");

          isRunning=false;
       }

       private void stopNow() {
          stopNow(true);
       }

       private void sbWrite(String ins) {
          sb.append(ins);
          result.setContent(sb.toString());
       }

       private String query(String instring) {
          result.setStatus("QUERY");
          result.setContent(instring);
          try {
             ServerSocket socket=new ServerSocket(54444);
             Socket socketclient=socket.accept();
  
             PrintWriter out = new PrintWriter(socketclient.getOutputStream(), true);
             BufferedReader in = new BufferedReader(new InputStreamReader(socketclient.getInputStream()));
             String inputLine, outputLine;
      
             outputLine = "HELLO!";
             out.println(outputLine);
      
             while ((inputLine = in.readLine()) != null) {
 
                  out.println(outputLine);
                  break;

             }
             out.close();
             in.close();
             socketclient.close();
             socket.close();
             return inputLine;
          } catch (Exception sockex) {
             return "ERROR";
          }

       }

       // Move file to sent directory
       private void moveToSent(File f) {
          f.renameTo(new File(f.getParent() + File.separator + "Sent" + File.separator + f.getName()));
       }

       private void safeSleep(int ms) {
          try {
             Thread.sleep(ms);
          } catch (Exception e) {
             // Do nothing
          }
       }

       // Comparator to reverse sort files by File Name (this will put dtdefid/auserid forms
       // in chronological order, newest to oldest.  Only the newest will be sent.
       private class ReverseSortFilesByName implements Comparator<File>{
          public int compare(File o1,File o2) {
             // reverse sort
             return o2.getName().compareTo(o1.getName());
          }
       }

    } 

}



