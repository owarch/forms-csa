 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/* 
 *
 * LoginController.java -  FORMS Login point
 *
 */

package com.owarchitects.forms.csa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.io.*;
import java.security.*;
import javax.crypto.SecretKey;
import javax.servlet.http.*;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.*;
import org.springframework.web.context.support.*;
import org.apache.axis2.context.*;
import org.apache.axis2.client.*;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.commons.io.FileUtils;
//import org.apache.commons.dbcp.BasicDataSource;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.orm.hibernate3.HibernateJdbcException;
import java.lang.reflect.*;
import org.springframework.beans.factory.config.BeanDefinition;

public class LoginController extends FORMSCsaServiceClientController {

   String worklocally;

   // Don't perform initial auth check;
   { 
      autoAuthCheckOff(); 
   }

   public ModelAndView submitRequest() throws FORMSException {

      // Get context
      HttpSession session=request.getSession();
      WinProps wp=SessionObjectUtil.getWinPropsObj(request,response);
      wp.setWinNameMissOk("N");

      try {

        // PrintWriter for diagnostic output
        PrintWriter out=response.getWriter();        

        // Initial Login Screen
        if (request.getParameter("directive")==null && session.getAttribute("encryptKey")==null) {

           try {

              // Verify database existance 
              EmbeddedusersDAO eusersDAO=(EmbeddedusersDAO)this.getContext().getBean("embeddedusersDAO");
              Embeddedusers e=eusersDAO.getRecord("XXXX");

           } catch (HibernateJdbcException hje) {

              exceptionRedirect(hje);

           } catch (org.springframework.transaction.CannotCreateTransactionException ccte) {

              exceptionRedirect(ccte);

           }

           // Continue
           String user=(String)session.getAttribute("user");
           if (user==null) user="";
           mav.addObject("user",user);
           mav.addObject("status","OK");
           return mav;

        // Verify Login Attempt
        } else if (request.getParameter("directive").equalsIgnoreCase("LOGIN")) {

           // Receive request parameters

           String user=request.getParameter("user");
           String passwd=request.getParameter("passwd");
           String status="";
           String encpw="";
           String dbpw="";
           worklocally=request.getParameter("worklocally");

           // Process user-suplied data

           EmbeddedusersDAO eusersDAO=(EmbeddedusersDAO)this.getContext().getBean("embeddedusersDAO");
                 
           Embeddedusers euser=eusersDAO.getRecord(user);
          
           if (euser!=null) {

              // Check if account locked
              if (euser.getIsdisabled()) {
                 
                 mav.addObject("status","LOCKED");
                 mav.addObject("badlogincount",-1);
                 return mav;
  
              }
  
              // See if password matches
              String pwck=PasswordUtil.pwEncrypt(passwd);
              if (pwck!=null && pwck.equals(euser.getPassword())) {

                 //////////////////////
                 // SUCCESSFUL LOGIN //
                 //////////////////////

                 // update bad login count
                 euser.setBadlogincount(new Integer(0));           
                 eusersDAO.saveOrUpdate(euser);
  
                 // Retrieve (optionally create) user's private encryption key 
                 PrivateKey privkey=EncryptUtil.getPrivateKey(session,user,passwd);
                    
                 if (privkey==null) {

                    mav.addObject("status","KEYERROR");
                    mav.addObject("badlogincount",-1);
                    return mav;
  
                 } 
                 
                 // Use private key to retrieve encryption, db passwords
  
                 encpw=EncryptUtil.decryptString(euser.getFkpassword(),privkey);;
                 dbpw=EncryptUtil.decryptString(euser.getDbpassword(),privkey);

                 // read maindb into contgext if necessary
                 LocalusersDAO lusersDAO=null;
                 try {
                    lusersDAO=(LocalusersDAO)this.getContext().getBean("localusersDAO");
                 } catch (org.springframework.beans.factory.NoSuchBeanDefinitionException nsbe) {
                    SessionObjectUtil.readMainDBIntoContext(getSession(),user,passwd);
                    lusersDAO=(LocalusersDAO)this.getContext().getBean("localusersDAO");
                 }
                 AllusersDAO ausersDAO=(AllusersDAO)this.getContext().getBean("allusersDAO");
                 Localusers luser=lusersDAO.getCurrentRecordByUsername(user);

                 // Retrieve encryption key
                 try {

                    SecretKey sk=EncryptUtil.getEncryptionKey(session,encpw);
                    // Write to session 
                    SessionObjectUtil.setEncryptionKeyObj(session,sk);
                    // Begin FORMSAuth object
                    FORMSAuth auth=this.getAuth();
                    Allusers auser=(Allusers)getMainDAO().execUniqueQuery(
                       "select a from Allusers a where a.luserid=" + luser.getLuserid() + " and " + 
                          "a.allinst.islocal=true" 
                       );
                    auth.setValue("username",user);
                    auth.setValue("userdesc",auser.getUserdesc());
                    auth.setValue("auserid",new Long(auser.getAuserid()).toString());
                    auth.setValue("luserid",new Long(luser.getLuserid()).toString());
                    auth.setValue("session","1");
  
                 } catch (FORMSNoEncryptKeyException ne) {

                    mav.addObject("status","NOENCKEY");
                    mav.addObject("badlogincount",-1);
                    return mav;
  
                 } catch (Exception keyx) {

                    mav.addObject("status","BADENCKEYPASS");
                    mav.addObject("badlogincount",-1);
                    return mav;
                 }

                 // Check if account is expired
                 try {
                    Date acctexpdate=DateUtils.truncate(luser.getAccountexpiredate(),Calendar.DATE);
                    if (acctexpdate!=null && new Date().after(acctexpdate)) {
                       logEvent("User login attempted - account expired");
                       mav.addObject("status","ACCTEXPIRED");
                       return mav;
                    }
                 } catch (Exception aee) {

                    // Do nothing - no account expiration date
                 }

                 this.getAuth().setValue("password",EncryptUtil.encryptString(passwd,
                    SessionObjectUtil.getInternalKeyObj(getSession())));

                 // LOGIN OK - REDIRECT USER
                 if (worklocally!=null && worklocally.equalsIgnoreCase("T")) {

                    getAuth().setValue("worklocally","T");
                    logEvent("User login");
                    initAuthValues();
                    clearStoredObjects();
                    safeRedirect("selectstudy.do");

                 } else {

                    // Check conectivity with SSA
                    boolean canconnect=false;
                    try {
                       CsaServiceSystemParms parms=new CsaServiceSystemParms();
                       parms.setBeanname("verifyConnectionServiceTarget");
                       parms.setMethodname("checkStatus");
                       CsaServiceStatusResult result=(CsaServiceStatusResult)this.submitServiceRequest(parms);
                       if (result.getStatus()!=null && result.getStatus()==FORMSServiceConstants.OK) {
                          canconnect=true;
                       }
                    } catch (Exception conne) { }

                    initAuthValues();
                    if (canconnect) {
                       getAuth().setValue("worklocally","F");
                       logEvent("User login");
                       mav.addObject("status","CALLUPDATEPROG");
                       return mav;
                    } else {
                       getAuth().setValue("worklocally","T");
                       logEvent("User login");
                       clearStoredObjects();
                       safeRedirect("selectstudy.do");
                    }

                 }
                 return null;
  
              } else {

                 // Check & update BadLoginCount, display result
                 euser.setBadlogincount(euser.getBadlogincount().intValue()+1);           

                 int badlogins=new Long(euser.getBadlogincount()).intValue();
                 int maxbad=SessionObjectUtil.getConfigObj(session).getMaxInvalidLogins();

                 // clear session attributes to enable correct behavior of LoginController

                 getSession().removeAttribute("sysconn");
                 getSession().removeAttribute("encryptKey");
                 getSession().removeAttribute("formsauth");

                 if (badlogins<maxbad) {
  
                    mav.addObject("status","FAILED");
                    mav.addObject("badlogincount",badlogins);
                    eusersDAO.saveOrUpdate(euser);
                    return mav;
  
                 } else if (badlogins==maxbad) {
  
                    mav.addObject("status","WARNING");
                    mav.addObject("badlogincount",badlogins);
                    eusersDAO.saveOrUpdate(euser);
                    return mav;
  
                 } else {
  
                    euser.setIsdisabled(true);
                    mav.addObject("status","EXCESSIVELOGINS");
                    mav.addObject("badlogincount",badlogins);
                    eusersDAO.saveOrUpdate(euser);
                    return mav;
  
                 }
  
              }
  
           } else {

              ///////////////////////////////////////////////////////////////////////////////
              // No such user found.  Attempt FORMSService connection to SSA to setup user //
              ///////////////////////////////////////////////////////////////////////////////

              // get SSA url
              SsainfoDAO ssainfoDAO=(SsainfoDAO)this.getContext().getBean("ssainfoDAO");
              Ssainfo si=ssainfoDAO.getRecord();
              long csaid=si.getCsaid();
              // Attempt registration with user credentials

              CsaServiceSystemParms parms=new CsaServiceSystemParms();
              parms.setUsername(user);
              parms.setPassword(passwd);
              parms.setCsaid(csaid);
              parms.setBeanname("csaRegisterUserServiceTarget");
              parms.setMethodname("register");
 
              CsaRegisterUserResult rur=null;
 
              // Return if problems connecting with SSA
              try {

                 try {
                    rur=(CsaRegisterUserResult)submitServiceRequest(parms,user,passwd);
                 } catch (ClassCastException cce) {

                    // LoginResult returned by FORMSServiceInterceptor when invalid username
                    mav.addObject("status","FAILED");
                    return mav;
                 } catch (Exception oce) {

                    // LoginResult returned by FORMSServiceInterceptor when invalid username
                    mav.addObject("status","NOSERVER");
                    return mav;
                 }

                 if (rur==null) {
                    // LoginResult returned by FORMSServiceInterceptor when invalid username
                    mav.addObject("status","FAILED");
                    return mav;
                 }

                 if (rur.getStatus()==FORMSServiceConstants.OK) {

                    ////////////////////////////
                    // Create encryption keys //
                    ////////////////////////////
       
                    // Create user account keypair
                    try {
                       EncryptUtil.createKeyPair(getSession(),user,passwd);
                    } catch (Exception e3) {
                       throw new FORMSException("Could not create user encryption keypair");
                    }

                    try {

                       // Retrieve formsinternal key
                       SecretKey formsinternal=EncryptUtil.getSecretKey(
                              getSession(),"formsinternal",SessionObjectUtil.getConfigObj(getSession()).getKeyStorePassword()
                                                                       );              
                       String fkpassword=EncryptUtil.decryptString(rur.getFkpassword(),formsinternal);
                       String dbpassword=EncryptUtil.decryptString(rur.getDbpassword(),formsinternal);
   
                       Embeddedusers newuser=new Embeddedusers();
                       newuser.setUsername(user);
                       newuser.setPassword(PasswordUtil.pwEncrypt(passwd));
                       newuser.setIsdisabled(false); 
                       newuser.setBadlogincount(0);
                       PublicKey pk=EncryptUtil.getPublicKey(getSession(),user,passwd);
                       PrivateKey privkey=EncryptUtil.getPrivateKey(getSession(),user,passwd);
                       newuser.setFkpassword(EncryptUtil.encryptString(fkpassword,pk));
                       newuser.setDbpassword(EncryptUtil.encryptString(dbpassword,pk));
                       eusersDAO.saveOrUpdate(newuser);
                       
                       // create sysconn read MainDB into context
                       String formstime=new Long(System.currentTimeMillis()).toString();
                       Sysconn sconn=new Sysconn();
                       sconn.setUsername(user);
                       sconn.setPassword(passwd);
                       sconn.setFormstime(formstime);
                       if (sconn.getUsername()!=null) {
                          sysconn=EncryptUtil.encryptString(ObjectXmlUtil.objectToXmlString(sconn),
                                              SessionObjectUtil.getInternalKeyObj(getSession()));
                          request.getSession().setAttribute("sysconn",sysconn);
                          try {
                             SessionObjectUtil.readMainDBIntoContext(session,user,passwd);
                          } catch (Exception cex) {
                             this.refreshContext();
                          }
                       } 

                       LocalusersDAO lusersDAO=(LocalusersDAO)this.getContext().getBean("localusersDAO");
                       AllusersDAO ausersDAO=(AllusersDAO)this.getContext().getBean("allusersDAO");
                       Localusers luser=lusersDAO.getCurrentRecordByUsername(user);
      
                       // Retrieve encryption key
                       try {
                          SecretKey sk=EncryptUtil.getEncryptionKey(session,fkpassword);
                          // Write to session 
                          SessionObjectUtil.setEncryptionKeyObj(session,sk);
                          // Begin FORMSAuth object
                          FORMSAuth auth=this.getAuth();
                          Allusers auser=(Allusers)getMainDAO().execUniqueQuery(
                             "select a from Allusers a where a.luserid=" + luser.getLuserid() + " and " + 
                                "a.allinst.islocal=true" 
                             );
                          auth.setValue("username",user);
                          auth.setValue("userdesc",auser.getUserdesc());
                          auth.setValue("auserid",new Long(auser.getAuserid()).toString());
                          auth.setValue("luserid",new Long(luser.getLuserid()).toString());
                          auth.setValue("session","1");
                          auth.setValue("password",EncryptUtil.encryptString(passwd,
                             SessionObjectUtil.getInternalKeyObj(getSession())));
                          initAuthValues();
                          logEvent("User login");
                          mav.addObject("status","CALLUPDATEPROG");
                          return mav;
                      
                       } catch (Exception authex) {
                          // do nothing, retry login
                       }

                       if (worklocally!=null && worklocally.equalsIgnoreCase("T")) {
                          getAuth().setValue("worklocally","T");
                       } else {
                          getAuth().setValue("worklocally","F");
                       }
                       initAuthValues();

                       clearStoredObjects();
                       safeRedirect("selectstudy.do");
                       return null;
                    } catch (Exception e3) {

                       throw new FORMSException("Could not register user with SSA");
                    }

                 } else {
                    mav.addObject("status","SERVICEINVALID");
                 }   
 
              } catch (Exception e2) {

                 out.println(Stack2string.getString(e2));
                 mav.addObject("status","SERVICEEXCEPTION");
 
              }
              return mav;

           }
           
        } 

      } catch (Exception e) {

        //throw new FORMSException("Login Controller Error",e);
        out.println(Stack2string.getString(e) + "<BR><BR>");
        out.println(this.getContext());
        return null;

      } 

      return null;

    }

    private void initAuthValues() throws FORMSException,FORMSKeyException,FORMSSecurityException {
           
       // Set apptype
       getAuth().setValue("apptype","CSA");
       // initialize study sort order
       this.getAuth().setValue("showarchivedstudies","false");
       this.getAuth().setValue("studysortcolumn","siteno");
       this.getAuth().setValue("studysortdir","asc");

    }

    private void exceptionRedirect(Exception e) throws FORMSException {
   
       Pattern p1=Pattern.compile("^.*cause: *Database.*not found\\..*$",Pattern.MULTILINE);
       Pattern p2=Pattern.compile("Connections could not be acquired from the underlying database!.*$",Pattern.MULTILINE);
       Matcher m1=p1.matcher(Stack2string.getString(e));
       Matcher m2=p2.matcher(Stack2string.getString(e));
       if (m1.find() || m2.find()) {
          safeRedirect("setup.do");
       } else {
          out.println(Stack2string.getString(e));
       }

    }

    private void clearStoredObjects() {
        // Clear out certain stored objects
        try {
        getAuth().setSessionObjectValue("remoteformsessionlist",null);
        getAuth().setSessionObjectValue("ftypespermlist",null);
        getAuth().setSessionObjectValue("rrolelist",null);
        getAuth().setSessionObjectValue("rroleidlist",null);
        getAuth().setSessionObjectValue("fpermlist",null);
        getAuth().setSessionObjectValue("mpermlist",null);
        getAuth().setSessionObjectValue("ftypespermlist",null);
        getAuth().setSessionObjectValue("gpermlist",null);
        getAuth().setSessionAttribute("formhalist",null);
        getAuth().setSessionAttribute("datatablehalist",null);
        getAuth().setSessionAttribute("dthalist",null);
        getAuth().setSessionAttribute("datatablelist",null);
        getAuth().setSessionAttribute("formtypeslist",null);
        getAuth().setSessionAttribute("mediahalist",null);
        getAuth().setSessionAttribute("mediainfolist",null);
        getAuth().setSessionAttribute("remotemediasessionlist",null);
        getAuth().setSessionAttribute("remotereportsessionlist",null);
        getAuth().setSessionObjectValue("anasyspermlist",null);
        getAuth().setSessionObjectValue("arolelist",null);
        getAuth().setSessionObjectValue("aroleidlist",null);
        getAuth().setSessionObjectValue("agrouplist",null);
        getAuth().setSessionObjectValue("agroupidlist",null);
        getAuth().setSessionObjectValue("apermlist",null);
        getAuth().setSessionObjectValue("ahapermlist",null);
        getAuth().setValue("rightslevel","");
        } catch (Exception e) { }
     }

}


