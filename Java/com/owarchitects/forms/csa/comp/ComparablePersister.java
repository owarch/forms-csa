 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/* 
 *
 * ComparablePersister - Used by data transfer classes to order EntityPersisters for transfer according
 *   to their internal constraints
 *
 */

package com.owarchitects.forms.csa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import org.hibernate.*;
import org.hibernate.persister.entity.*;

public class ComparablePersister implements Comparable {

   EntityPersister persister;
   String name;
   int sortvalue;

   ComparablePersister(EntityPersister persister,SessionFactory sessionFactory) {
      this.persister=persister;
      Session session = sessionFactory.openSession();
      this.name=persister.getEntityName();
      session.close();
      assignSortValue();
   }

   public EntityPersister getPersister() {
      return persister;
   }

   public String getName() {
      return name;
   }

   public int getSortvalue() {
      return sortvalue;
   }

   private void assignSortValue() {
      if (name.indexOf("Datatablecoldef_transfer")>=0) sortvalue=10;

      else if (name.indexOf("Formstore_transfer")>=0) sortvalue=9;

      else if (name.indexOf("Formtypes_transfer")>=0) sortvalue=8;

      else if (name.indexOf("Dtuserpermassign_transfer")>=0) sortvalue=7;

      else if (name.indexOf("Formattrs_transfer")>=0) sortvalue=6;
      else if (name.indexOf("Formtypeuserpermassign_transfer")>=0) sortvalue=6;

      else if (name.indexOf("Allusers_transfer")>=0) sortvalue=5;
      else if (name.indexOf("Rroleuserassign_transfer")>=0) sortvalue=5;
      else if (name.indexOf("Datatabledef_transfer")>=0) sortvalue=5;


      else if (name.indexOf("Dtmodtime_transfer")>=0) sortvalue=4;

      else if (name.indexOf("Formha_transfer")>=0) sortvalue=3;
      else if (name.indexOf("Datatableha_transfer")>=0) sortvalue=3;
      else if (name.indexOf("Formtypelist_transfer")>=0) sortvalue=3;
      else if (name.indexOf("Linkedstudies_transfer")>=0) sortvalue=3;

      else if (name.indexOf("Userinfomodtime_transfer")>=0) sortvalue=2;
      else if (name.indexOf("Dtrolepermprofassign_transfer")>=0) sortvalue=2;
      else if (name.indexOf("Fileextensions_transfer")>=0) sortvalue=2;
      else if (name.indexOf("Dtpermprofassign_transfer")>=0) sortvalue=2;
      else if (name.indexOf("Formtyperolepermassign_transfer")>=0) sortvalue=2;
      else if (name.indexOf("Studies_transfer")>=0) sortvalue=2;
      else if (name.indexOf("Allinst_transfer")>=0) sortvalue=2;
      else if (name.indexOf("Useridassign_transfer")>=0) sortvalue=2;

      else if (name.indexOf("Formformats_transfer")>=0) sortvalue=1;
      else if (name.indexOf("Datatableformats_transfer")>=0) sortvalue=1;
      else sortvalue=0;
   }

   public int compareTo(Object other) {
      ComparablePersister oclass=(ComparablePersister)other;
      if (this.sortvalue>oclass.getSortvalue()) {
         return +1;
      } else if (this.sortvalue<oclass.getSortvalue()) {
         return -1;
      } else if (this.name.compareTo(oclass.getName())>0) {
         return +1;
      } else {
         return -1;
      }
   }   

}   


