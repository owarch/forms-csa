 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/* 
 *
 * FORMSCsaServiceClientController.java -  Extends FORMSController to include Service helpers
 *
 */

package com.owarchitects.forms.csa.comp;

import com.owarchitects.forms.commons.db.*;
import com.owarchitects.forms.commons.comp.*;

import java.util.*;
import java.io.*;
import java.beans.*;
import java.lang.reflect.*;

//import org.apache.axis2.context.*;
//import org.apache.axis2.client.*;
//import org.apache.axis2.client.async.AxisCallback;
//import org.apache.axis2.transport.http.HTTPConstants;
//import org.apache.axis2.AxisFault;
//import org.apache.axis2.addressing.EndpointReference;
//import org.apache.axis2.rpc.client.RPCServiceClient;
//import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import javax.xml.namespace.QName;

import javax.net.ssl.*;
import javax.servlet.http.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public abstract class FORMSCsaServiceClientController extends FORMSServiceClientController {

   // Submit Service Request (Override method)
   public FORMSServiceResult submitServiceRequest(FORMSServiceParms parms) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      return CsaServiceClientUtil.submitServiceRequest(getSession(),getContext(),getAuth(),getSyswork(),
            parms,CsaServiceClientUtil.getSsaUrl(getSession(),getContext(),getAuth()) + "/formsserviceinterceptor.do");

   }   

   // Simple Service Request (pass single object)
   public FORMSServiceResult submitServiceRequest(String bean,String method,Object obj) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      CsaServicePassObjectParms parms=new CsaServicePassObjectParms(bean,method);
      parms.setObject(obj);
      return submitServiceRequest(parms);

   }   

   // Submit service request (uname,passwd supplied)
   public FORMSServiceResult submitServiceRequest(FORMSServiceParms parms,String username,String password) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      return CsaServiceClientUtil.submitServiceRequest(getSession(),getContext(),getSyswork(),
            parms,username,password,CsaServiceClientUtil.getSsaUrl(getSession(),getContext()) + "/formsserviceinterceptor.do");

   }   

   // Submit SOAP Request (Override method)
   public FORMSServiceResult submitSoapRequest(FORMSServiceParms parms) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      return CsaServiceClientUtil.submitSoapRequest(getSession(),getContext(),getAuth(),getSyswork(),
            parms,CsaServiceClientUtil.getSsaUrl(getSession(),getContext(),getAuth()) + "/services/FORMSServiceInterceptor");

   }   
   
   // Retrieve SSA URL
   public String getSsaUrl() throws FORMSException,FORMSKeyException,FORMSSecurityException { 
      return CsaServiceClientUtil.getSsaUrl(getSession(),getContext(),getAuth());
   }


   // SSA QUERY METHODS

   protected List execServerQuery(String queryString) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      ServerQueryParms parms=new ServerQueryParms();
      parms.setQuerystring(queryString);
      return execServerQuery(parms);
   }

   protected List execServerQuery(String queryString,String[] names,Object[] values) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      ServerQueryParms parms=new ServerQueryParms();
      parms.setQuerystring(queryString);
      parms.setParmnames(names);
      parms.setParmvalues(values);
      return execServerQuery(parms);
   }

   private List execServerQuery(ServerQueryParms parms) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      parms.setBeanname("csaServerQueryServiceTarget");
      parms.setMethodname("execServerQuery");
      CsaServiceListResult result=(CsaServiceListResult)submitServiceRequest(parms);
      return result.getList();
   }

   protected Object[] execUniqueServerQuery(String queryString) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      ServerQueryParms parms=new ServerQueryParms();
      parms.setQuerystring(queryString);
      return execUniqueServerQuery(parms);
   }

   protected Object[] execUniqueServerQuery(String queryString,String[] names,Object[] values) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      ServerQueryParms parms=new ServerQueryParms();
      parms.setQuerystring(queryString);
      parms.setParmnames(names);
      parms.setParmvalues(values);
      return execUniqueServerQuery(parms);
   }

   private Object[] execUniqueServerQuery(ServerQueryParms parms) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      parms.setBeanname("csaServerQueryServiceTarget");
      parms.setMethodname("execUniqueServerQuery");
      CsaServiceObjectResult result=(CsaServiceObjectResult)submitServiceRequest(parms);
      try {
         return (Object[])result.getObject();
      } catch (Exception e) {
         Object[] oarray=new Object[1];
         oarray[0]=(Object)result.getObject();
         return oarray;
      }
   }

   // REMOTE SSA QUERY METHODS

   protected List execRemoteServerQuery(Linkedinst inst,String queryString) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      ServerQueryParms parms=new ServerQueryParms();
      parms.setLinkedinst(inst);
      parms.setQuerystring(queryString);
      return execRemoteServerQuery(parms);
   }

   protected List execRemoteServerQuery(Linkedinst inst,String queryString,String[] names,Object[] values) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      ServerQueryParms parms=new ServerQueryParms();
      parms.setLinkedinst(inst);
      parms.setQuerystring(queryString);
      parms.setParmnames(names);
      parms.setParmvalues(values);
      return execRemoteServerQuery(parms);
   }

   private List execRemoteServerQuery(ServerQueryParms parms) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      parms.setBeanname("csaServerQueryServiceTarget");
      parms.setMethodname("execRemoteServerQuery");
      CsaServiceListResult result=(CsaServiceListResult)submitServiceRequest(parms);
      return result.getList();
   }

   protected Object[] execUniqueRemoteServerQuery(Linkedinst inst,String queryString) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      ServerQueryParms parms=new ServerQueryParms();
      parms.setLinkedinst(inst);
      parms.setQuerystring(queryString);
      return execUniqueRemoteServerQuery(parms);
   }

   protected Object[] execUniqueRemoteServerQuery(Linkedinst inst,String queryString,String[] names,Object[] values) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      ServerQueryParms parms=new ServerQueryParms();
      parms.setLinkedinst(inst);
      parms.setQuerystring(queryString);
      parms.setParmnames(names);
      parms.setParmvalues(values);
      return execUniqueRemoteServerQuery(parms);
   }

   private Object[] execUniqueRemoteServerQuery(ServerQueryParms parms) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      parms.setBeanname("csaServerQueryServiceTarget");
      parms.setMethodname("execUniqueRemoteServerQuery");
      CsaServiceObjectResult result=(CsaServiceObjectResult)submitServiceRequest(parms);
      try {
         return (Object[])result.getObject();
      } catch (Exception e) {
         Object[] oarray=new Object[1];
         oarray[0]=(Object)result.getObject();
         return oarray;
      }
   }

}


