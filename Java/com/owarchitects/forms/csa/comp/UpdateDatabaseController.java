 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/* 
 *
 * UpdateDatabaseController.java -  CSA Database Update Program
 *
 *   This class extends FORMSServiceClientController rather than FORMSCsaServiceClientController because it
 *   is a non-standard CSA controller in the sense that it is called outside a CSA
 *   logged in session
 *
 */

package com.owarchitects.forms.csa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.io.*;
import java.lang.*;
import java.security.*;
import javax.crypto.SecretKey;
import javax.servlet.http.*;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.*;
import org.springframework.web.context.support.*;
//import org.apache.axis2.context.*;
//import org.apache.axis2.client.*;
//import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.commons.io.FileUtils;
//import org.apache.commons.dbcp.BasicDataSource;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.orm.hibernate3.HibernateJdbcException;
import org.apache.tools.ant.*;
import org.springframework.orm.hibernate3.LocalSessionFactoryBean;
import org.hibernate.*;
import org.hibernate.metadata.*;
import org.springframework.beans.factory.FactoryBean; 
import java.lang.reflect.*;
import org.hibernate.persister.entity.*;

public class UpdateDatabaseController extends FORMSCsaServiceClientController {

   private String url;
   private String user;
   private String passwd;
   private boolean needsync;
   private boolean isRunning=false;
   private boolean alreadyAsked=false;

   public ModelAndView submitRequest() throws FORMSException {

      try {

         user=getAuth().getValue("username");
         passwd=EncryptUtil.decryptString(getAuth().getValue("password"),
                  SessionObjectUtil.getInternalKeyObj(getSession()));
         url=getSsaUrl();
   
         //run();
         if (!isRunning) {
            // See if sync directory contains unsyncedfiles
            needsync=false;
            try {
               String syncdirpath = getSession().getServletContext().getRealPath("/") + File.separator + 
                                    "SyncFiles";
               Iterator i=Arrays.asList(new File(syncdirpath).listFiles()).iterator();                     
               while (i.hasNext()) {   
                  File f=(File)i.next();
                  if (f.isFile() && f.getName().toLowerCase().indexOf("xml.encrypt")>0) {
                     needsync=true;
                     break;
                  }
               }
            } catch (Exception syncex) {
               // do nothing
            }
            // Start database update thread
            UpdateThread proc=new UpdateThread(getMainDAO());
            proc.start();
         }   
   
         mav.addObject("status","THREAD");
         mav.addObject("needsync",needsync);
         return mav;

      } catch (Exception e) {
         // Don't perform update
         safeRedirect("selectstudy.do");
         return null;
      }

    }


    // UpdateThread - Performs actual setup as a thread

    private class UpdateThread extends Thread {

       com.owarchitects.forms.commons.db.MainDAO maindao=null;
       IframeReloaderResult result;
       StringBuilder sb;

       public UpdateThread(com.owarchitects.forms.commons.db.MainDAO maindao) {
          this.maindao=maindao;
       }

       public void run() {

          if (isRunning==true) return;

          alreadyAsked=false;
          isRunning=true;
          sb=new StringBuilder();
          result=new IframeReloaderResult();
          // Only need to set this once, since iframeresult will hold reference to object
          // rather than copy of it
          getSession().setAttribute("iframeresult",result);

          try {

             result.setStatus("RELOAD");

             sb.append("\n<span style='color:#00AA00'><br>Begin processing:<br><br></span>\n");

             //////////////////////////////////////////////
             // GENERATE LIST OF TABLES REQUIRING UPDATE //
             //////////////////////////////////////////////

             sbWrite("Checking SSA Server for Database Updates: <br><br>\n");
             List locallist=maindao.getTable("Tablemodtime");
             List localsavelist=Arrays.asList(locallist.toArray());
             List remotelist=getRemoteTable("com.owarchitects.forms.commons.db.Tablemodtime");
             List newtablemod=(List)new ArrayList();

             Iterator r=remotelist.iterator();
             Iterator l=null;
             boolean needrebuild=false;
             while (r.hasNext()) {
                Tablemodtime rmod=(Tablemodtime)r.next();
                if (rmod.getClassname().indexOf("Systemlog")>=0 || rmod.getClassname().indexOf("Tablemodtime")>=0) {
                   // Systemlog is maintained separately on client
                   r.remove();
                   continue;
                }
                l=locallist.iterator();
                boolean hasmatch=false;
                boolean needupdate=false;
                while (l.hasNext()) {
                   Tablemodtime lmod=(Tablemodtime)l.next();
                   if (lmod.getClassname().replaceAll("_transfer","").equals(rmod.getClassname().replaceAll("_transfer",""))) {
                      hasmatch=true;
                      Date lmodtime=lmod.getTablemodtime();
                      if (lmodtime==null) lmodtime=new Date(new Long(0).longValue());
                      Date rmodtime=rmod.getTablemodtime();
                      if (rmodtime==null) rmodtime=new Date(new Long(0).longValue());
                      if (lmodtime.equals(rmodtime)!=true) {
                         needupdate=true;
                      }
                      // Systemlog is maintained separately on client
                      if (lmod.getClassname().indexOf("Systemlog")>=0) {
                         l.remove();
                         break;
                      }
                      sbWrite(lmod.getClassname() + " &nbsp;&nbsp;&nbsp; " + lmodtime + " &nbsp;&nbsp;&nbsp;  " +
                              rmodtime + " &nbsp;&nbsp;&nbsp;  " + needupdate + "<br>\n");
                      l.remove();
                      break;
                   }
                }
                if (hasmatch && !needupdate) {
                   // remove from remotelist records not needing update
                   r.remove();
                } else if (!hasmatch) {
                   // add to newtablemod records which don't have a local match
                   newtablemod.add(rmod);
                } 
             }





             // create ordered remotelist to minimize objects with unmet dependencies
             List sortedlist=new ArrayList();
             Iterator i=remotelist.iterator();
             while (i.hasNext()) {
                sortedlist.add(new ComparableModTable((Tablemodtime)i.next()));
             }

             /////////////////////////////////////////////////////////////////////////
             // Add local tablemod records for new records in remote tablemod table //
             /////////////////////////////////////////////////////////////////////////

             Iterator n=newtablemod.iterator();
             while (n.hasNext()) {
                Tablemodtime rmod=(Tablemodtime)n.next();
                Tablemodtime t=new Tablemodtime();
                t.setClassname(rmod.getClassname());
                t.setTablemodtime(new Date(new Long(0).longValue()));
                maindao.saveOrUpdate(t);
             }

             //////////////////////////////////////////////////////////////////////////
             // REMOVE RECORDS FROM LOCAL DATABASE THAT NO LONGER EXIST IN REMOTE DB // 
             //////////////////////////////////////////////////////////////////////////

             boolean unmetdep=false; 
             r=sortedlist.iterator();
             // hold list for records that cannot be removed from local list
             List cantrlist=(List)new ArrayList();
             while (r.hasNext()) {
                ComparableModTable rmodtable=(ComparableModTable)r.next();
                Tablemodtime rmod=rmodtable.getModTable();
                sbWrite(".....DONE<br><br>\n");
                String classname=rmod.getClassname() + "_transfer";
                sbWrite("Updating Table " + classname + "...<br>\n");
                sbWrite("Removing outdated records - TABLE" + classname + "...<br>\n");

                // Pull list of remote ID's //
                List idlist=getRemoteIdentifiers(classname);
                // PUll list of local ID's //
                List llist=null;
                try {
                   llist=maindao.getTableIdentifiers(classname);
                } catch (Exception qse) {
                   sbWrite("NOTE:  Data for this table is not transferred to CSA - Nothing Done - Table update complete.<br>");
                   continue;
                }
                // Find local ID's without corresponding remote ID's //
                Iterator liter=llist.iterator();
                boolean anyfail=false;
                while (liter.hasNext()) {
                   Serializable lid=(Serializable)liter.next();
                   boolean hasmatch=false;
                   Iterator iiter=idlist.iterator();
                   while (iiter.hasNext()) {
                      Serializable cid=(Serializable)iiter.next();
                      // for some bizarre reason in this section of code, comparisons had to be written (ckval==true) rather
                      // than if (ckval) or if (!ckval) or if (lid.equals(cid)).  Not sure what's going on with this but
                      // once the comparisons were written if (boolean==true) or (boolean==false) the section of code
                      // worked.
                      boolean ckval=lid.equals(cid);
                      if (ckval==true) {
                         hasmatch=true;
                         iiter.remove();
                      }
                   }
                   if (hasmatch==false) {
                      Object o=maindao.get(Class.forName(classname),lid);
                      if (o!=null) {
                         sbWrite("Deleting " +  o.toString() + "<br>");
                         try {
                            maindao.delete(o);
                         } catch (Exception cde) {
                            cantrlist.add(o);
                         }
                      } else {
                         sbWrite("ERROR:  Couldn't pull record for deletion - will try to continue");
                      }
                   }
                }
                if (anyfail==false) {
                   sbWrite(".....DONE<br><br>\n");
                } else {
                   sbWrite("<br>&nbsp;&nbsp;&nbsp;&nbsp;NOTE:  Some targeted objects hav unmet dependencies to be resolved later." +
                             "&nbsp;&nbsp;&nbsp;&nbsp;<br>\n");
                }                    
             }
             // Remove objects that couldn't be removed because of unmet dependencies //
             if (cantrlist.size()>0) {
                sbWrite("Removing objects with previously unmet dependencies.\n");
             }
             int ivar=0;
             while (cantrlist.size()>0) {
                sbWrite("<br>&nbsp;&nbsp;&nbsp;Remaining Objects:  " + cantrlist.size());
                ivar++;
                Iterator niter=cantrlist.iterator();
                int prevcount=cantrlist.size();
                while (niter.hasNext()) {
                    Object rmvobj=(Object)niter.next();
                    try {
                       maindao.delete(rmvobj);
                       niter.remove();
                    } catch (Exception e) {
                    }
                }
                if (cantrlist.size()==prevcount) {
                   throw new FORMSException("Could not resolve all dependencies for removal.  Will try to proceed....<br><br>");
                };
             } 

             ///////////////////////////////////////////////////////////////////////
             // ADD NEW/UPDATE EXISTING LOCAL OBJECTS WITH OBJECTS FROM REMOTE DB //
             ///////////////////////////////////////////////////////////////////////

             r=sortedlist.iterator();
             ArrayList notyet=new ArrayList();
             while (r.hasNext()) {
                ComparableModTable rmodtable=(ComparableModTable)r.next();
                Tablemodtime rmod=rmodtable.getModTable();
                String classname=rmod.getClassname() + "_transfer";
                sbWrite("Updating Table " + classname + "...<br>\n");
                sbWrite("Integrating updated records...\n\n");
                // pull most recent modification time
                List updatelist=null;
                java.sql.Timestamp tablemodtime=null;
                Iterator liter=localsavelist.iterator();
                while (liter.hasNext()) {
                   Tablemodtime lmod=(Tablemodtime)liter.next();
                   if (lmod.getClassname().equals(classname)) {
                      tablemodtime=new java.sql.Timestamp(lmod.getTablemodtime().getTime());
                   }
                }

                Object o=null;
                try {
                   o=maindao.execUniqueQuery("select max(recmodtime) from " + classname);
                } catch (Exception qse) {
                   sbWrite("NOTE:  Data for this table is not transferred to CSA - Nothing Done - Table update complete.<br>");
                   continue;
                }
                if (o!=null) {
                   java.sql.Timestamp maxrecmod=(java.sql.Timestamp)o;
                   if (tablemodtime==null || maxrecmod.before(tablemodtime)) {
                      tablemodtime=maxrecmod;
                   }
                }

                if (tablemodtime==null) {
                   updatelist=getRemoteTable(classname);
                } else {
                   try {
                      updatelist=execRemoteQuery("select r from " + classname + " r where r.recmodtime > :tablemodtime",
                         new String[] { "tablemodtime" },new Object[] { tablemodtime } );
                   } catch (Exception ule) {
                      updatelist=getRemoteTable(classname);
                   } 
                } 
                if (updatelist!=null) {
                   sbWrite("<br>\n     # of records for update=" + updatelist.size());
                   sbWrite("<br>\n     Begin updating records....");
                   Iterator u=updatelist.iterator();
                   while (u.hasNext()) {
                      while (u.hasNext()) {
                         Object saveobj=(Object)u.next();
                         java.util.Date orimodtime=null;
                         try {
                             // keep original record modification time incase save fails
                            Method gmeth=saveobj.getClass().getMethod("getRecmodtime");
                            orimodtime=(java.util.Date)gmeth.invoke(saveobj,new Object[0]);
                         } catch (Exception ome) { }
                         try {
                            // clear child class sets - unmet dependencies impair persistence 
                            // and child sets not important for parent persistence
                            Method[] smethods=saveobj.getClass().getDeclaredMethods();
                            Iterator miter=Arrays.asList(smethods).iterator();
                            Set nullset=(Set)new HashSet();
                            while (miter.hasNext()) {
                               Method m=(Method)miter.next();
                               Class[] p=m.getParameterTypes();
                               if (p.length==1 && (p[0].getName().equals("Set") ||
                                     p[0].getName().equals("java.util.Set"))) { 
                                  m.invoke(saveobj,new Object[] { nullset });      
                               }
                            }
                            maindao.saveOrUpdate(saveobj);
                         } catch (Exception e) {
                            unmetdep=true;
                            if (orimodtime!=null) {
                               // reset modification time
                               try {
                                   // keep original record modification time incase save fails
                                  Method smeth=saveobj.getClass().getMethod("setRecmodtime",java.util.Date.class);
                                  smeth.invoke(saveobj,orimodtime);
                               } catch (Exception ome) { }
                            }
                            notyet.add(saveobj);
                         }
                      }
                      if (unmetdep) {
                         sbWrite("&nbsp;&nbsp;&nbsp;&nbsp;NOTE:  Table has unmet dependencies to be resolved later." +
                                    "&nbsp;&nbsp;&nbsp;&nbsp;<br>\n");
                      }
                   }
                }   
                sbWrite(".....DONE<br><br>\n");
             }
             if (notyet.size()>0) {
                sbWrite("Persisting objects with previously unmet dependencies.\n");
             }
             ivar=0;
             while (notyet.size()>0) {
                sbWrite("<br>&nbsp;&nbsp;&nbsp;Remaining Objects:  " + notyet.size());
                ivar++;
                Iterator niter=notyet.iterator();
                int prevcount=notyet.size();
                while (niter.hasNext()) {
                    Object saveobj=(Object)niter.next();
                    java.util.Date orimodtime=null;
                    try {
                        // keep original record modification time incase save fails
                       Method gmeth=saveobj.getClass().getMethod("getRecmodtime");
                       orimodtime=(java.util.Date)gmeth.invoke(saveobj,new Object[0]);
                    } catch (Exception ome) { }

                    try {

                       maindao.saveOrUpdate(saveobj);
                       niter.remove();
        
                    } catch (Exception e) {

                        // reset modification time
                        try {
                            // keep original record modification time incase save fails
                           Method smeth=saveobj.getClass().getMethod("setRecmodtime",java.util.Date.class);
                           smeth.invoke(saveobj,orimodtime);
                        } catch (Exception ome) {
                        }

                    }
                }
                if (notyet.size()==prevcount) {

                   throw new FORMSException("Could not resolve database dependencies.  Database could not be completely copied");

                };

             } 

             /////////////////////////
             // Update Tablemodtime //
             /////////////////////////
             r=sortedlist.iterator();
             if (sortedlist.size()>0) {
                sbWrite("Updating table modifed time for updated tables<br>\n");
                locallist=maindao.getTable("Tablemodtime");
             }
             while (r.hasNext()) {
                ComparableModTable rmodtable=(ComparableModTable)r.next();
                Tablemodtime rmod=rmodtable.getModTable();
                l=locallist.iterator();
                while (l.hasNext()) {
                   Tablemodtime lmod=(Tablemodtime)l.next();
                   if (lmod.getClassname().replaceAll("_transfer","").equals(rmod.getClassname().replaceAll("_transfer",""))) {
                      Date rmodtime=rmod.getTablemodtime();
                      Date lmodtime=lmod.getTablemodtime();
                      lmod.setTablemodtime(rmodtime);
                      maindao.saveOrUpdate(lmod);
                   }
                }
             }
             sbWrite(".....DONE<br><br>\n");
             sbWrite("\nCSA database update is complete<br><br>\n");
             sbWrite("\n<script>\n");
             sbWrite("\n//parent.document.getElementById('okbutn').click()\n");
             sbWrite("\n</script>\n");
             
          } catch (Exception e) {

             sbWrite("<span style='color:#AA0000'><br><br>Warning!!! Process threw an exception. &nbsp;" +
                       "Please check URL and internet connection.<br><br><span style='color:#000000'>" +
                       Stack2string.getString(e) + "</span><br><br>" +
                       "<br><br></span>\n");
             stopNow(false);
             return;

          }

          stopNow(true);
          return;

       }

       private List getRemoteTable(String tname) throws FORMSException {

          CsaDataTransferParms cdtp=new CsaDataTransferParms();
          cdtp.setUsername(user);
          cdtp.setPassword(passwd);
          cdtp.setBeanname("csaDataTransferServiceTarget");
          cdtp.setMethodname("getTable");
          cdtp.setMappingClassName(tname);
          
          CsaDataTransferResult cdtr=null;
 
          cdtr=(CsaDataTransferResult)submitServiceRequest(cdtp,url + "/formsserviceinterceptor.do");
          return cdtr.getResultlist();

       }         

       private List getRemoteIdentifiers(String tname) throws FORMSException {

          CsaDataTransferParms cdtp=new CsaDataTransferParms();
          cdtp.setUsername(user);
          cdtp.setPassword(passwd);
          cdtp.setBeanname("csaDataTransferServiceTarget");
          cdtp.setMethodname("getIdentifiers");
          cdtp.setMappingClassName(tname);
          
          CsaDataTransferResult cdtr=null;
 
          cdtr=(CsaDataTransferResult)submitServiceRequest(cdtp,url + "/formsserviceinterceptor.do");
          return cdtr.getResultlist();

       }         

       private List execRemoteQuery(String querystring) throws FORMSException {

          CsaDataTransferParms cdtp=new CsaDataTransferParms();
          cdtp.setUsername(user);
          cdtp.setPassword(passwd);
          cdtp.setBeanname("csaDataTransferServiceTarget");
          cdtp.setMethodname("execQuery");
          cdtp.setQuerystring(querystring);
          
          CsaDataTransferResult cdtr=null;
 
          cdtr=(CsaDataTransferResult)submitServiceRequest(cdtp,url + "/formsserviceinterceptor.do");
          return cdtr.getResultlist();

       }         

       private List execRemoteQuery(String querystring,String[] parmnames,Object[] parmvalues) throws FORMSException {

          CsaDataTransferParms cdtp=new CsaDataTransferParms();
          cdtp.setUsername(user);
          cdtp.setPassword(passwd);
          cdtp.setBeanname("csaDataTransferServiceTarget");
          cdtp.setMethodname("execQuery");
          cdtp.setQuerystring(querystring);
          cdtp.setParmnames(parmnames);
          cdtp.setParmvalues(parmvalues);
          
          CsaDataTransferResult cdtr=null;
 
          cdtr=(CsaDataTransferResult)submitServiceRequest(cdtp,url + "/formsserviceinterceptor.do");
          return cdtr.getResultlist();

       }         

       private void stopNow(boolean isok) {
          if (isok) {
             sb.append("<span style='color:#00AA00'>CSA setup completed successfully!<br><br></span>\n");
          } else {
             sb.append("<span style='color:#AA0000'>Setup process was unsuccessful<br><br></span>\n");
          }
          // Make ok button visible
          sb.append("<script>\n");
          sb.append("   var b=parent.document.getElementById('okbutn');\n");
          sb.append("   b.style.visibility='visible';\n");
          if (isok && !alreadyAsked) {
              alreadyAsked=true;
              sb.append("   b.click();\n");
          }
          sbWrite("</script>\n");

          result.setContent(sb.toString());
          if (!isok) {
             result.setStatus("RELOAD");
             safeSleep(500);
          }
          result.setStatus("DONE");
          isRunning=false;
       }

       private void sbWrite(String ins) {
          sb.append(ins);
          result.setContent(sb.toString());
       }

       private void safeSleep(int ms) {
          try {
             Thread.sleep(ms);
          } catch (Exception e) {
             // Do nothing
          }
       }

    } 


    public class ComparableModTable implements Comparable {
    
       Tablemodtime modtable;
       String classname;
       int sortvalue;
    
       ComparableModTable(Tablemodtime modtable) {
          this.modtable=modtable;
          this.classname=modtable.getClassname();
          assignSortValue();
       }
    
       public Tablemodtime getModTable() {
          return modtable;
       }
    
       public String getClassname() {
          return classname;
       }
    
       public int getSortvalue() {
          return sortvalue;
       }
    
       private void assignSortValue() {
          if (classname.indexOf("Datatablecoldef_transfer")>=0) sortvalue=10;
    
          else if (classname.indexOf("Formstore_transfer")>=0) sortvalue=9;
    
          else if (classname.indexOf("Formtypes_transfer")>=0) sortvalue=8;
    
          else if (classname.indexOf("Dtuserpermassign_transfer")>=0) sortvalue=7;
    
          else if (classname.indexOf("Formattrs_transfer")>=0) sortvalue=6;
          else if (classname.indexOf("Formtypeuserpermassign_transfer")>=0) sortvalue=6;
    
          else if (classname.indexOf("Allusers_transfer")>=0) sortvalue=5;
          else if (classname.indexOf("Rroleuserassign_transfer")>=0) sortvalue=5;
          else if (classname.indexOf("Datatabledef_transfer")>=0) sortvalue=5;
    
    
          else if (classname.indexOf("Dtmodtime_transfer")>=0) sortvalue=4;
    
          else if (classname.indexOf("Formha_transfer")>=0) sortvalue=3;
          else if (classname.indexOf("Datatableha_transfer")>=0) sortvalue=3;
          else if (classname.indexOf("Formtypelist_transfer")>=0) sortvalue=3;
          else if (classname.indexOf("Linkedstudies_transfer")>=0) sortvalue=3;
    
          else if (classname.indexOf("Userinfomodtime_transfer")>=0) sortvalue=2;
          else if (classname.indexOf("Dtrolepermprofassign_transfer")>=0) sortvalue=2;
          else if (classname.indexOf("Fileextensions_transfer")>=0) sortvalue=2;
          else if (classname.indexOf("Dtpermprofassign_transfer")>=0) sortvalue=2;
          else if (classname.indexOf("Formtyperolepermassign_transfer")>=0) sortvalue=2;
          else if (classname.indexOf("Studies_transfer")>=0) sortvalue=2;
          else if (classname.indexOf("Allinst_transfer")>=0) sortvalue=2;
          else if (classname.indexOf("Useridassign_transfer")>=0) sortvalue=2;
    
          else if (classname.indexOf("Formformats_transfer")>=0) sortvalue=1;
          else sortvalue=0;
       }
    
       public int compareTo(Object other) {
          ComparableModTable oclass=(ComparableModTable)other;
          if (this.sortvalue>oclass.getSortvalue()) {
             return +1;
          } else if (this.sortvalue<oclass.getSortvalue()) {
             return -1;
          } else if (this.classname.compareTo(oclass.getClassname())>0) {
             return +1;
          } else {
             return -1;
          }
       }   
    
    }   

}





